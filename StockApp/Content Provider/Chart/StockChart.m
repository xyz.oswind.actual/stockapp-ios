//
//  StockChart.m
//  StockApp
//
//  An object that performs symbol chart retrieval and notifies interested
//  controllers when this task is completed using the StockChartDelegate
//  protocol. The connections are performed asynchronously using NSURLSession.
//  This object is the delegate for all NSURLSession used.
//
//  -- Implementation Notes ----------------------------------------------------
//  This class uses Yahoo! Finance for symbol lookup. The Yahoo URL used is:
//  https://chart.finance.yahoo.com/z?s=QUERY&t=1d&q=l&l=on&z=l&a=v&p=s
//  where `s=QUERY` is the symbol of the chart to retrieve.
//
//  Once the NSURLSession completes the response data contains raw image data.
//  ----------------------------------------------------------------------------
//
//  Created by David McKnight on 2016-03-04.
//  Copyright (C) 2016 David McKnight actual@oswind.xyz
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#import "StockChart.h"

#pragma mark - Private constants

/* Local constants declaring error codes for NSURLSession. */
static const int SESSION_CANCELLED = -999;
/* Local constants declaring the partial strings of a stock chart query. */
static const NSString * chartUrlStart = @"https://chart.finance.yahoo.com/z?s=";
static const NSString * chartUrlEnding = @"&t=1d&q=l&l=on&z=l&p=s";

#pragma mark - Private declaration

@interface StockChart()

/* Contains the query response data obtained from the server. */
@property (nonatomic, strong) NSMutableData * responseData;

/* Contains the current query session. May not be running or valid. */
@property (nonatomic, strong) NSURLSession * statusSession;

/* Get an URL corresponding to a current chart query. The parameter `symbol`
 * should contain a valid stock symbol. No checks for validity of the symbol
 * are performed. */
- (NSURL *) getURLForSymbol:(NSString *)symbol;

/* Create a NSURLSession using the NSURL passed. Any existing sessions will be
 * cancelled to prevent multiple queries being in an incomplete state. */
- (void) createSessionWithURL:(NSURL *)url;

/* A check for stock chart has failed due to a client-side error. Notify any
 * interested controllers of the failure and provide the NSError object. */
- (void) notifyOnSessionFailedWithError:(NSError *)error;

/* A check for stock chart has completed. Validate response data and notify any
 * interested controllers of the chart being available. */
- (void) notifyOnChartAvailable;

@end

#pragma mark - Public implementation

@implementation StockChart

/* Convenience method which accepts one parameter `symbol`. The symbol provided
 * must exist otherwise this method will not return any useful information. The
 * symbol provided is used to obtain the current chart of the stock. Once the
 * current chart is retrieved it becomes available via the public property
 * defined above. */
- (void) getChartWithSymbol:(NSString *)symbol {
    /* Guard against a chart query with empty symbols. */
    if (![symbol isEqualToString:@""] && symbol != nil) {
        dispatch_queue_t chart = dispatch_queue_create("chart", NULL);
        /* Dispatch a status thread. */
        dispatch_async(chart, ^{
            /* Create the NSURL using the provided symbol. */
            NSURL * url = [self getURLForSymbol:symbol];
            /* Request current status data. */
            [self createSessionWithURL:url];
        });
    }
}

/* Get an URL corresponding to a current chart query. The parameter `symbol`
 * should contain a valid stock symbol. No checks for validity of the symbol
 * are performed. */
- (NSURL *) getURLForSymbol:(NSString *)symbol {
    /* The string representation of the URL. */
    NSString * urlString = [NSString stringWithFormat:@"%@%@%@",
                            chartUrlStart, symbol, chartUrlEnding];
    return [NSURL URLWithString:urlString];
}

/* Create a NSURLSession using the NSURL passed. Any existing sessions will be
 * cancelled to prevent multiple queries being in an incomplete state. */
- (void) createSessionWithURL:(NSURL *)url {
    /* Cancel any existing sessions that have not yet completed. */
    [self.statusSession invalidateAndCancel];
    /* Create a new aysynchronous session with default caching and cookies. */
    _statusSession = [NSURLSession sessionWithConfiguration:
                      [NSURLSessionConfiguration defaultSessionConfiguration]
                            delegate:self delegateQueue:nil];
    /* Create the HTTPS GET request with the specified URL. */
    NSURLSessionDataTask * task = [self.statusSession dataTaskWithURL:url];
    /* Perform the HTTPS GET request. */
    [task resume];
}

/* A check for stock chart has failed due to a client-side error. Notify any
 * interested controllers of the failure and provide the NSError object. */
- (void) notifyOnSessionFailedWithError:(NSError *)error {
    /* A session that completes with an error is considered by interested
     * controllers to be a failure if the error is not `cancelled`. A
     * cancelled session is handled in didBecomeInvalidWithError. */
    if (error != NULL && error.code != SESSION_CANCELLED) {
        NSLog(@"A session failed with client-side error code: %ld, "
              "and description: %@", (long)error.code,
              error.localizedDescription);
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.delegate chartDidNotRetrieveWithError:error];
        });
    }
}

/* A check for stock chart has completed. Validate response data and notify any
 * interested controllers of the chart being available.*/
- (void) notifyOnChartAvailable {
    /* Make the chart data available on public-readonly property chartData. */
    _chartData = self.responseData;
    /* Notify interested controllers that the chart is available. */
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.delegate chartDidRetrieve];
    });
}

#pragma mark - NSURLSessionDataDelegate

/* NSURLSession delegate for handling the initial server response. */
- (void)URLSession:(NSURLSession *)session
        dataTask:(NSURLSessionDataTask *)dataTask
        didReceiveResponse:(NSURLResponse *)response
        completionHandler:
            (void (^)(NSURLSessionResponseDisposition disposition))
            completionHandler {
    /* Allow all connection requests. */
    completionHandler(NSURLSessionResponseAllow);
    /* Reallocate response data ivar for each new connection. */
    _responseData = [[NSMutableData alloc] init];
}

/* NSURLSession delegate for handling when the application receives data. */
- (void)URLSession:(NSURLSession *)session
        dataTask:(NSURLSessionDataTask *)dataTask
        didReceiveData:(NSData *)data {
    /* Append the new data to the ivar. */
    [self.responseData appendData:data];
}

#pragma mark - NSURLSessionTaskDelegate

/* NSURLSession delegate for handling when a task is completed. Notify any
 * interested controllers of a failure or process the response data. */
- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task
        didCompleteWithError:(NSError *)error {
    /* Will be NULL if no error (not error code zero) occurred. Cancelled
     * sessions have an error code of -999. Any other error should be passed to
     * interested controllers. */
    if (error == NULL) {
        /* Notify delegate of the available status. */
        [self notifyOnChartAvailable];
    } else {
        /* Notify delegate as required when an error occurs. */
        [self notifyOnSessionFailedWithError:error];
    }
}

#pragma mark - NSURLSessionDelegate

/* NSURLSession delegate for handling a connection becoming invalidated. Notify
 * interested controllers if the invalidation was not explicitly requested. A
 * session will be explicitly invalidated to prevent overlapping queries to the
 * server. Most likely this means the query has been changed before the session
 * completed for the last query requested. See also createSessionWithURL. */
- (void)URLSession:(NSURLSession *)session
        didBecomeInvalidWithError:(NSError *)error {
    /* When error is not nil the session has not been explicitly invalidated by
     * sending a cancel message. */
    if (error != nil) {
        NSLog(@"A session was invalidated with client-side error code: %ld, "
              "and description: %@", (long)error.code,
              error.localizedDescription);
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.delegate chartDidNotRetrieveWithError:error];
        });
    }
}

@end
