//
//  StockChart.h
//  StockApp
//
//  An object that performs symbol chart retrieval and notifies interested
//  controllers when this task is completed using the StockChartDelegate
//  protocol. The connections are performed asynchronously using NSURLSession.
//  This object is the delegate for all NSURLSession used.
//
//  -- Implementation Notes ----------------------------------------------------
//  This class uses Yahoo! Finance for symbol lookup. The Yahoo URL used is:
//  https://chart.finance.yahoo.com/z?s=QUERY&t=1d&q=l&l=on&z=l&a=v&p=s
//  where `s=QUERY` is the symbol of the chart to retrieve.
//
//  Once the NSURLSession completes the response data contains raw image data.
//  ----------------------------------------------------------------------------
//
//  Created by David McKnight on 2016-03-04.
//  Copyright (C) 2016 David McKnight actual@oswind.xyz
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#import <Foundation/Foundation.h>
#import "StockChartDelegate.h"

#pragma mark - Public declaration

@interface StockChart : NSObject
<NSURLSessionDelegate, NSURLSessionTaskDelegate, NSURLSessionDataDelegate>

/* Notify a controller that implements the StockChartDelegate protocol when
 * stock chart queries complete or when errors occur. */
@property (nonatomic,strong) id <StockChartDelegate> delegate;

/* The chart data for use with image views. */
@property (nonatomic,strong,readonly) NSData * chartData;

/* Convenience method which accepts one parameter `symbol`. The symbol provided
 * must exist otherwise this method will not return any useful information. The
 * symbol provided is used to obtain the current chart of the stock. Once the
 * current chart is retrieved it becomes available via the public property 
 * defined above. */
- (void) getChartWithSymbol:(NSString *)symbol;

@end
