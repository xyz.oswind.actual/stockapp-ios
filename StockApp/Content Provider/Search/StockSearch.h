//
//  StockSearch.h
//  StockApp
//
//  An object that performs symbol lookups and notifies interested controllers
//  when this task is completed using the StockSearchDelegate protocol. The
//  connections are performed asynchronously using NSURLSession. This object is
//  the delegate for all NSURLSession used.
//
//  -- Implementation Notes ----------------------------------------------------
//  This class uses Yahoo! Finance for symbol lookup. The Yahoo URL used is:
//  https://ca.finance.yahoo.com/lookup/stocks?s=QUERY&t=S&b=0&m=ALL
//  where `s=QUERY` is the symbol or description to search for, `t=S` represents
//  a search for stocks, `b=0` is the offset into results (only useful if total
//  results exceeds 20), and `m=ALL` represents the market to search (ALL, or US
//  for US and Canada).
//
//  The results are pre-filtered and converted to JSON using YQL:
//  https://developer.yahoo.com/yql/ where the select string used is either
//
//  (A) For symbol lookup using <tr> tags of class yui-dt-odd or yui-dt-even.
//  select * from html where url='YAHOO_URL_ABOVE' and xpath=
//  '//tr[contains(@class,"yui-dt-")]'
//
//  -or-
//
//  (B) For obtaining total results using <li> tags of class selected.
//  select * from html where url='YAHOO_URL_ABOVE' and xpath=
//  '//li[contains(@class,"selected")]//a/em'
//
//  Finally, once the JSON object is returned, it is converted to Foundation
//  objects by selecting the appropriate keys or key paths. In general this
//  means less data is read by the iOS device and less local processing is
//  required. Thereby reducing expensive wireless radio use and cpu time.
//  ----------------------------------------------------------------------------
//
//  Created by David McKnight on 2016-02-10.
//  Copyright (C) 2016 David McKnight actual@oswind.xyz
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#import <Foundation/Foundation.h>
#import "StockSearchDelegate.h"

#pragma mark - Public constants

/* Public constants declaring stock-provider specific parameters. */
extern const int YAHOO_MAX_PER_REQUEST; /* 20 */

#pragma mark - Public declaration

@interface StockSearch : NSObject
<NSURLSessionDelegate, NSURLSessionTaskDelegate, NSURLSessionDataDelegate>

/* When a search query is completed make it available to any controller. This
 * pointer may be passed to controllers implementing the StockSearchDelegate. It
 * is readonly to make the results available without risk of corruption. The
 * symbols are stored as symbol-description pairs. */
@property (nonatomic,readonly,strong) NSArray * symbols;

/* When searching for stocks this property indicates if results should be 
 * retured from world markets. Default is Yes. A setting of No returns stocks
 * from local (US & Canada) markets. */
@property (atomic) BOOL queryWorldMarkets;

/* Notify a controller that implements the StockSearchDelegate protocol when
 * symbol queries complete or when errors occur. */
@property (nonatomic,strong) id <StockSearchDelegate> delegate;

/* The total number of results loadable into the table view. This may not be
 * the same as the current number of results currently loaded into `symbols`. */
@property (atomic,readonly) int totalResults;

/* Setup the default object state on creation. */
- (id) init;

/* Perform a stock symbol lookup using the query string passed. The string
 * parameter `query` can contain a stock description, a partial stock symbol, or
 * a complete stock symbol to search for. */
- (void) performSearchWithString:(NSString *)query;

/* Obtain a stock entry at an offset into available symbols. The parameter
 * `index` contains the offset into available symbols requested. Returns the
 * entry found as a symbol-description pair or nil if it was out-of-bounds. */
- (NSArray *) getStockAtIndex:(long)index;

/* Obtain the stock symbol at an offset into available symbols. The parameter
 * `index` contains the offset into available symbols requested. Returns the
 * NSString representation of the stock symbol. */
- (NSString *) getSymbolAtIndex:(long)index;

/* Obtain the stock description at an offset into available symbols. The
 * parameter `index` contains the offset into available symbols requested.
 * Returns the NSString representation of the stock description. */
- (NSString *) getDescriptionAtIndex:(long)index;

/* Request the next window of results using the last query provided. If more
 * results were previously found this will create an NSURLSession containing
 * the query string for the next set of results. The results will then be loaded
 * into responseData for extracting symbols. If no more results were found this
 * method will do nothing. Controllers must have implemented the 
 * StockSearchDelegate protocol to be notified of completion. */
- (void) requestNextResults;

@end
