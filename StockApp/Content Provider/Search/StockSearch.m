//
//  StockSearch.m
//  StockApp
//
//  An object that performs symbol lookups and notifies interested controllers
//  when this task is completed using the StockSearchDelegate protocol. The
//  connections are performed asynchronously using NSURLSession. This object is
//  the delegate for all NSURLSession used.
//
//  -- Implementation Notes ----------------------------------------------------
//  This class uses Yahoo! Finance for symbol lookup. The Yahoo URL used is:
//  https://ca.finance.yahoo.com/lookup/stocks?s=QUERY&t=S&b=0&m=ALL
//  where `s=QUERY` is the symbol or description to search for, `t=S` represents
//  a search for stocks, `b=0` is the offset into results (only useful if total
//  results exceeds 20), and `m=ALL` represents the market to search (ALL, or US
//  for US and Canada).
//
//  The results are pre-filtered and converted to JSON using YQL:
//  https://developer.yahoo.com/yql/ where the select string used is either
//
//  (A) For symbol lookup using <tr> tags of class yui-dt-odd or yui-dt-even.
//  select * from html where url='YAHOO_URL_ABOVE' and xpath=
//  '//tr[contains(@class,"yui-dt-")]'
//
//  -or-
//
//  (B) For obtaining total results using <li> tags of class selected.
//  select * from html where url='YAHOO_URL_ABOVE' and xpath=
//  '//li[contains(@class,"selected")]//a/em'
//
//  Finally, once the JSON object is returned, it is converted to Foundation
//  objects by selecting the appropriate keys or key paths. In general this
//  means less data is read by the iOS device and less local processing is
//  required. Thereby reducing expensive wireless radio use and cpu time.
//  ----------------------------------------------------------------------------
//
//  Created by David McKnight on 2016-02-10.
//  Copyright (C) 2016 David McKnight actual@oswind.xyz
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#import "StockSearch.h"

#pragma mark - Public constants

/* Public constants declaring stock-provider specific parameters. */
const int YAHOO_MAX_PER_REQUEST = 20;

#pragma mark - Private constants

/* Local constants declaring error codes for NSURLSession. */
static const int SESSION_CANCELLED = -999;
/* Local constants declaring the partial strings of a stock symbol lookup-
 * specific query. These are expected to be the same for all symbol lookups. See
 * getFormattedUrlStringWithQuery for how the string is joined. */
static const NSString * queryUrlStart = @"https://query.yahooapis.com/v1/public"
    "/yql?q=select%20*%20from%20html%20where%20url%3D'https%3A%2F%2Fca.finance."
    "yahoo.com%2Flookup%2Fstocks%3Fs%3D";
static const NSString * queryUrlPreIndex = @"%26t%3DS%26b%3D";
static const NSString * queryUrlPostIndexPreMarket = @"%26m%3D";
static const NSString * queryUrlPreMarket = @"%26t%3DS%26m%3D";
static const NSString * queryUrlPostMarket = @"'%20and%20xpath%3D";
static const NSString * queryUrlEnding = @"&format=json&env=store%3A%2F%2Fdatat"
    "ables.org%2Falltableswithkeys";
/* Local Xpath constants for `search query` or for `total results`. */
static const NSString * queryUrlXpathSearch = @"'%2F%2Ftr%5Bcontains(%40class%2"
    "C%22yui-dt-%22)%5D'";
static const NSString * queryUrlXpathTotal = @"'%2F%2Fli%5Bcontains(%40class%2C"
    "%22selected%22)%5D%2F%2Fa%2Fem'";
/* Local market constants for worldwide or local (US and Canada) markets. */
static const NSString * queryUrlWorldMarkets = @"ALL";
static const NSString * queryUrlLocalMarkets = @"US";

#pragma mark - Private declaration

@interface StockSearch()

/* Contains the query response data obtained from the server. */
@property (nonatomic, strong) NSMutableData * responseData;

/* Contains the current query session. May not be running or valid. */
@property (nonatomic, strong) NSURLSession * searchSession;

/* Contains the current state. For simplicity the StockSearch object can be
 * reading `stock symbol` data or `total stock symbols` data. If the state is NO
 * then responseData contains unprocessed `stock symbol` data. If the state is
 * is YES then responseData contains unprocessed `total stock symbols` data. */
@property (atomic) BOOL stateIsReadingTotals;

/* A controller using StockSearch can issue multiple requests for more results.
 * Keep track of when the session is active in particular for requesting more 
 * results. This will guard against overlapping requests for more results. */
@property (atomic) BOOL moreResultsSessionIsBusy;

/* Hang onto the current formatted query string. It may be needed for multiple
 * requests such as extra results and obtaining totals for a query. */
@property (nonatomic,strong) NSString * currentFormattedQuery;

/* Contains the offset into totalResults for requesting more results. */
@property (atomic) NSUInteger indexIntoResults;

/* Get an URL corresponding to a stock symbol lookup. Internally uses the query
 * string which is expected to have already been reformatted as required (using
 * reformatQueryString) and set to currentFormattedQuery. Also internally uses
 * the current index into the results (default zero unless indexIntoResults is
 * updated). The parameter `xpath` must be one of either queryUrlXpathTotal or
 * queryUrlXpathSearch for the URL to be valid. Returns an NSURL initialized
 * with the server URL to complete a stock symbol lookup. */
- (NSURL *) getURLForCurrentQueryWithXpath:(const NSString *)xpath;

/* Based on the current state of the public property `queryWorldMarkets` obtain
 * the market specifier to use in query strings. */
- (const NSString *) getURLMarketSpecifier;

/* Apply any required fixes to a query string. Convert any characters that (1)
 * violate HTML specifications, or (2) would otherwise cause the search to fail.
 * The string parameter `query` can contain a stock description, a partial stock
 * symbol, or a complete stock symbol to search for. Reformatting requires 
 * converting problem characters to the HTML entity `&nbsp;`. This ensures 
 * compatibility with lookup engines that support embedded spaces. It also 
 * prevents spaces from breaking queries in general (at least in the case of 
 * Yahoo!). Most other edge-cases for characters will be discarded by the 
 * NSURLSession as an unsupported URL error. This may require additional 
 * adjustments for international localization. */
- (NSString *) reformatQueryString:(NSString *)query;

/* Create a NSURLSession using the URL passed. Any existing sessions will be
 * cancelled to prevent multiple queries being in an incomplete state. */
- (void) createSessionWithURL:(NSURL *)url;

/* Extract stock symbols and their descriptions from the query response data.
 * The NSData parameter `data` contains the unfiltered response from a completed
 * query session. Returns a NSArray object initialized with symbol-description
 * pairs of possible matches to the query performed. */
- (NSArray *) getSymbolsFromResponse:(NSData *)data;

/* A check for stock symbols using the last query has completed. Notify any
 * interested controllers of the completion and provide a pointer to the
 * current results. */
- (void) notifyOnResults;

/* A check for stock symbols has failed due to a client-side error. Notify any
 * interested controllers of the failure and provide the NSError object. */
- (void) notifyOnSessionFailedWithError:(NSError *)error;

/* Rows extracted from response data using valueForKey may contain objects of
 * type NSNull instead of NSString. The parameter `row` contains a html row from
 * the response data. Check the row and replace any instances of NSNull with
 * empty strings. This ensures that any objects the rely on the processed
 * symbols will get strings consistently. Returns a NSArray containing the
 * symbol-description pair in string format. */
- (NSArray *) processResultWithRow:(NSArray *)row;

/* Obtain the total number of stocks returned by a symbol lookup query. By
 * default the source (Yahoo) returns 20 symbols at a time. This is used to
 * obtain the maximum number of symbols that can be shown for a query. Note
 * this is intended to be called either before or after the actual results. It
 * shouldn't be called in parallel because it shares responseData and will
 * overwrite any unprocessed results. It also doesn't perform any additional
 * checks on the validity of query (not null or empty). */
- (void) getTotalResultsWithCurrentQuery;

/* Obtain the complete string representation of an URL. This is intended to be
 * used prior to creating the URL. The parameter `query` is expected to have
 * already been reformatted if required (using reformatQueryString). Returns
 * the result of joining URL partial strings. This is required to create the
 * NSURL representation for a NSURLSession. */
- (NSString *) getUrlStringWithQuery:(NSString *)query
                            AndIndex:(NSUInteger)index
                           AndMarket:(const NSString *)market
                            AndXpath:(const NSString *)xpath;

/* The count contained in a JSON formatted result is the authoritative count of
 * useable objects described. */
- (int) getCountFromResponse:(NSData *)data;

/* Obtain the total number of stock symbols that can be returned from a GET.
 * This is based on the query performed and facilitates lazy loading. The
 * parameter `data` must contain response data from a query that used
 * queryUrlXpathTotal otherwise this method will return zero. The total is
 * returned as a string object. */
- (NSString *) getTotalFromResponse:(NSData *)data;

/* Check if the last query submitted has more results than the current symbols
 * found. Reuses the last formatted query with queryUrlXpathTotal to obtain
 * a concrete number of symbols that can be lazy loaded. */
- (void) checkLastQueryForMoreResults;

/* A check for more results using the last query has completed. Notify any
 * interested controllers if more symbol results were found. */
- (void) notifyOnTotalResults;

/* When a request for more results is made the current index is incremented.
 * However the next index may not be in range. Returns YES if the next index
 * is still in range of available results. Otherwise returns NO. */
- (BOOL) nextIndexIsInRange;

/* Restore the internal state for processing response data to defaults. */
- (void) resetInternalState;

/* A NSURLSession has completed and response data contains initial results.
 * Extract initial results and notify any interested controllers. */
- (void) processInitialResults;

/* A NSURLSession has completed and response data contains more results. These
 * results need to be extracted and appended to existing results. Notify any
 * interested controllers when this task is complete. */
- (void) processMoreResults;

@end

#pragma mark - Public implementation

@implementation StockSearch

/* Setup the default object state on creation. */
- (id) init {
    if (self = [super init]) {
        /* Unless set otherwise all queries search world markets. */
        _queryWorldMarkets = YES;
        /* The initial state is reading stock symbols into responseData. */
        self.stateIsReadingTotals = NO;
        /* Initialize total results and index into results. */
        _totalResults = 0; /* Using ivar for public read-only property. */
        self.indexIntoResults = 0;
        /* Default state is to allow a `more results` request to start. */
        self.moreResultsSessionIsBusy = NO;
    }
    return self;
}

/* Perform a stock symbol lookup using the query string passed. The string
 * parameter `query` can contain a stock description, a partial stock symbol, or
 * a complete stock symbol to search for. */
- (void) performSearchWithString:(NSString *)query {
    /* Guard against searching with empty queries. */
    if (![query isEqualToString:@""] && query != nil) {
        /* A search was requested so reset current state. */
        [self resetInternalState];
        dispatch_queue_t search = dispatch_queue_create("search", NULL);
        /* Dispatch a search thread. */
        dispatch_async(search, ^{
            /* Hang onto the formatted query as it may be needed again. */
            self.currentFormattedQuery = [self reformatQueryString:query];
            NSURL * url = /* Using the Xpath for symbol search. */
                [self getURLForCurrentQueryWithXpath:queryUrlXpathSearch];
            [self createSessionWithURL:url];
        });
    }
}

/* Get an URL corresponding to a stock symbol lookup. Internally uses the query
 * string which is expected to have already been reformatted as required (using
 * reformatQueryString) and set to currentFormattedQuery. Also internally uses 
 * the current index into the results (default zero unless indexIntoResults is
 * updated). The parameter `xpath` must be one of either queryUrlXpathTotal or
 * queryUrlXpathSearch for the URL to be valid. Returns an NSURL initialized 
 * with the server URL to complete a stock symbol lookup. */
- (NSURL *) getURLForCurrentQueryWithXpath:(const NSString *)xpath {
    /* The formatted string representation of the URL. */
    NSString * urlString = /* Use the Xpath for a symbol search or totals. */
        [self getUrlStringWithQuery:self.currentFormattedQuery
                           AndIndex:self.indexIntoResults
                          AndMarket:[self getURLMarketSpecifier]
                           AndXpath:xpath]; /* See constants declarations. */
    /* The NSURL representation of the url string. */
    return [NSURL URLWithString:urlString];
}

/* Based on the current state of the public property `queryWorldMarkets` obtain
 * the market specifier to use in query strings. */
- (const NSString *) getURLMarketSpecifier {
    if (self.queryWorldMarkets == YES) {
        return queryUrlWorldMarkets; /* Queries search in world markets. */
    } else {
        return queryUrlLocalMarkets; /* Queries search local markets. */
    }
}

/* Apply any required fixes to a query string. Convert any characters that (1)
 * violate HTML specifications, or (2) would otherwise cause the search to fail.
 * The string parameter `query` can contain a stock description, a partial stock
 * symbol, or a complete stock symbol to search for. Reformatting requires
 * converting problem characters to the HTML entity `&nbsp;`. This ensures
 * compatibility with lookup engines that support embedded spaces. It also
 * prevents spaces from breaking queries in general (at least in the case of
 * Yahoo!). Most other edge-cases for characters will be discarded by the
 * NSURLSession as an unsupported URL error. This may require additional
 * adjustments for international localization. */
- (NSString *) reformatQueryString:(NSString *)query {
    /* The set of characters known to require re-formatting. */
    NSCharacterSet * remove = [NSCharacterSet
                               characterSetWithCharactersInString:@"\t.- "];
    /* The reformatted query string. */
    return [[query componentsSeparatedByCharactersInSet:remove]
            componentsJoinedByString:@"%26nbsp%3B"]; /* &nbsp; */
}

/* Create a NSURLSession using the URL passed. Any existing sessions will be
 * cancelled to prevent multiple queries being in an incomplete state. */
- (void) createSessionWithURL:(NSURL *)url {
    /* Cancel any existing sessions that have not yet completed. */
    [self.searchSession invalidateAndCancel];
    /* Create a new aysynchronous session with default caching and cookies. */
    _searchSession = [NSURLSession sessionWithConfiguration:
        [NSURLSessionConfiguration defaultSessionConfiguration]
        delegate:self delegateQueue:nil];
    /* Create the HTTPS GET request with the specified URL. */
    NSURLSessionDataTask * task = [self.searchSession dataTaskWithURL:url];
    /* Perform the HTTPS GET request. */
    [task resume];
}

/* Extract stock symbols and their descriptions from the query response data.
 * The NSData parameter `data` contains the unfiltered response from a completed
 * query session. Returns a NSArray object initialized with symbol-description
 * pairs of possible matches to the query performed. */
- (NSArray *) getSymbolsFromResponse:(NSData *)data {
    /* Store extracted stock symbols and descriptions. */
    NSMutableArray * extractedSymbols = [[NSMutableArray alloc] init];
    /* Extract the number of symbols returned from the response data. The 
     * response data contains the authoritative count of results. This is
     * important because the response data changes structure in a subtle way
     * depending on if multiple results are returned. Counting the results using
     * NSJSONSerialization followed by valueForKeyPath is not always accurate.
     * The use of valueForKeyPath can then be adjusted accordingly. */
    int count = [self getCountFromResponse:data];
    /* Only proceed if results were found. */
    if (count > 0) {
        /* Key path for row extraction is based on the count of results. 
         * Originally this was set to always extract the row key path. When a
         * single result is in the response data the <td> is not a valid key 
         * path. For a single result either use the key path below or check for
         * a single result and extract the data using valueForKey (and not using
         * valueForKeyPath). */
        NSString * keyPath = count > 1 ?
            @"query.results.tr": @"query.results.tr.td"; /* multiple: single */
        /* Extract html rows from the response data. */
        NSArray * htmlRows =
            [[NSJSONSerialization JSONObjectWithData:data
                options:NSJSONReadingMutableContainers error:NULL]
                    valueForKeyPath:keyPath];
        /* Extract symbols and descriptions from the html rows. */
        if (count == 1) {
            /* Response data contains a single result. Process the result. */
            NSArray * result = [self processResultWithRow:htmlRows];
            [extractedSymbols addObject:result];
        } else {
            /* Response data contains multiple results. Iterate over them. */
            for (NSArray * row in htmlRows) {
                /* Process each row in the results by data key path. */
                NSArray * result =
                    [self processResultWithRow:[row valueForKeyPath:@"td"]];
                [extractedSymbols addObject:result];
            }
        }
    }
    /* Return the filtered data consisting of symbol-description pairs. */
    return extractedSymbols;
}

/* Rows extracted from response data using valueForKey may contain objects of
 * type NSNull instead of NSString. The parameter `row` contains a html row from
 * the response data. Check the row and replace any instances of NSNull with
 * empty strings. This ensures that any objects the rely on the processed
 * symbols will get strings consistently. Returns a NSArray containing the
 * symbol-description pair in string format. */
- (NSArray *) processResultWithRow:(NSArray *)row {
    NSString * symbol; NSString * description;
    /* Check the symbol. */
    if ([[row[0] valueForKeyPath:@"a.content"]
         isKindOfClass:[NSString class]]) {
        symbol = [row[0] valueForKeyPath:@"a.content"];
    } else {
        NSLog(@"Response data contains an unset symbol.");
        symbol = @"";
    }
    /* Check the description. */
    if ([row[1] isKindOfClass:[NSString class]]) {
        description = row[1];
    } else {
        NSLog(@"Response data contains an unset description.");
        description = @"";
    }
    /* The processed row, a symbol and description as NSString objects. */
    return [NSArray arrayWithObjects:symbol, description, nil];
}

/* Obtain the total number of stocks returned by a symbol lookup query. By
 * default the source (Yahoo) returns 20 symbols at a time. This is used to
 * obtain the maximum number of symbols that can be shown for a query. Note
 * this is intended to be called either before or after the actual results. It
 * shouldn't be called in parallel because it shares responseData and will
 * overwrite any unprocessed results. It also doesn't perform any additional
 * checks on the validity of query (not null or empty). */
- (void) getTotalResultsWithCurrentQuery {
    /* Update the current processing state. Switch to reading real total. */
    self.stateIsReadingTotals = YES;
    dispatch_queue_t totals = dispatch_queue_create("totals", NULL);
    /* Dispatch a totals thread. */
    dispatch_async(totals, ^{
        NSURL * url = /* Using the Xpath for total results. */
            [self getURLForCurrentQueryWithXpath:queryUrlXpathTotal];
        [self createSessionWithURL:url];
    });
}

/* The count contained in a JSON formatted result is the authoritative count of
 * useable objects described. */
- (int) getCountFromResponse:(NSData *)data {
    return [[[NSJSONSerialization JSONObjectWithData:data
        options:NSJSONReadingMutableLeaves error:NULL]
             valueForKeyPath:@"query.count"] intValue];
}

/* Obtain the complete string representation of an URL. This is intended to be
 * used prior to creating the URL. The parameter `query` is expected to have
 * already been reformatted if required (using reformatQueryString). Returns
 * the result of joining URL partial strings. This is required to create the 
 * NSURL representation for a NSURLSession. */
- (NSString *) getUrlStringWithQuery:(NSString *)query
                            AndIndex:(NSUInteger)index
                           AndMarket:(const NSString *)market
                            AndXpath:(const NSString *)xpath {
    /* The joined string representation of the URL. Generally formed as:
     * queryUrlStart,
     * QUERY,
     * queryUrlPreIndex,
     * INDEX,
     * queryUrlPostIndexPreMarket,
     * MARKET,
     * queryUrlPostMarket,
     * XPATH,
     * queryUrlEnding 
     * See also constants declarations. */
    NSString * joined = [NSString stringWithFormat:@"%@%@%@%lu%@%@%@%@%@",
                            queryUrlStart,
                            query,
                            queryUrlPreIndex,
                            (unsigned long)index,
                            queryUrlPreMarket,
                            market,
                            queryUrlPostMarket,
                            xpath,
                            queryUrlEnding];
    return joined;
}

/* Obtain the total number of stock symbols that can be returned from a GET.
 * This is based on the query performed and facilitates lazy loading. The 
 * parameter `data` must contain response data from a query that used
 * queryUrlXpathTotal otherwise this method will return zero. The total is
 * returned as a string object. */
- (NSString *) getTotalFromResponse:(NSData *)data {
    /* Select the content key path which contains the total symbols found. Then
     * convert this result to a string representation. */
    NSString * content =
        [[NSJSONSerialization JSONObjectWithData:self.responseData
                                         options:NSJSONReadingMutableLeaves
                                           error:NULL]
         valueForKeyPath:@"query.results.em.content"];
    /* The value to extract is enclosed in parenthesis. */
    NSCharacterSet * delimiter = [NSCharacterSet
                                  characterSetWithCharactersInString:@"()"];
    /* The content string split by parenthesis. */
    NSArray * splitString =
        [content componentsSeparatedByCharactersInSet:delimiter];
    /* Guard against returning nil total. */
    if (splitString == nil || splitString[1] == nil) {
        NSLog(@"Total symbols was requested but got nil.");
        return @"";
    }
    /* The total is at offset [1]. */
    return splitString[1];
}

/* Check if the last query submitted has more results than the current symbols
 * found. Reuses the last formatted query with queryUrlXpathTotal to obtain
 * a concrete number of symbols that can be lazy loaded. */
- (void) checkLastQueryForMoreResults {
    /* Now that some results have been processed request the total
     * number of results that exist. Only do this if the source-specific
     * maximum has been reached (for Yahoo this is 20). */
    if (self.symbols.count == YAHOO_MAX_PER_REQUEST) {
        /* Check for more results because there might be more. */
        [self getTotalResultsWithCurrentQuery];
    } else if (self.symbols.count < YAHOO_MAX_PER_REQUEST) {
        /* There are no more symbols. */
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.delegate didAlreadyGetTotalResultsForLastQuery];
        });
    }
}

/* A check for stock symbols using the last query has completed. Notify any
 * interested controllers of the completion and provide a pointer to the
 * current results. The local property responseData contains stock symbols. */
- (void) notifyOnResults {
    /* Two kinds of results needs to be processed. The initial window and more
     * results. If a more results session is not busy then the response data
     * contains initial results. Otherwise response data contains more results
     * to be processed. */
    if (!self.moreResultsSessionIsBusy) {
        /* A more results session is not active. This indicates response data 
         * contains unprocessed initial results. The session has completed and 
         * is ready for further processing. Extract the initial results then
         * notify any interested controllers. */
        [self processInitialResults];
    } else {
        /* A more results session is active. This indicates response data 
         * contains new unprocessed stock symbols. Concatenate original results
         * and the new results then notify any interested controllers. */
        [self processMoreResults];
    }
}

/* A NSURLSession has completed and response data contains initial results.
 * Extract initial results and notify any interested controllers. */
- (void) processInitialResults {
    /* Extract the symbols from the response data. */
    _symbols = [self getSymbolsFromResponse:self.responseData];
    /* Save the starting offset into results. */
    self.indexIntoResults = 0;
    /* Symbols have been extracted. Notify interested controllers. */
    dispatch_async(dispatch_get_main_queue(), ^{
        /* Provide the current symbols found. */
        [self.delegate searchDidCompleteWithSymbols:self.symbols];
    });
    /* The first symbols have been reported. Now check if more exist. */
    [self checkLastQueryForMoreResults];
}

/* A NSURLSession has completed and response data contains more results. These
 * results need to be extracted and appended to existing results. Notify any
 * interested controllers when this task is complete. */
- (void) processMoreResults {
    /* Append the new results to existing results. */
    NSArray * moreResults = [self.symbols arrayByAddingObjectsFromArray:
                             [self getSymbolsFromResponse:self.responseData]];
    /* Update the symbols property to point to concatenated results. */
    _symbols = moreResults;
    /* Dispatch an update notification to interested controllers. */
    dispatch_async(dispatch_get_main_queue(), ^{
        /* Notify delegate of more results. */
        [self.delegate moreResultsDidCompleteWithMore];
        /* Re-enable more results requests. */
        self.moreResultsSessionIsBusy = NO;
    });

}

/* A check for stock symbols has failed due to a client-side error. Notify any
 * interested controllers of the failure and provide the NSError object. */
- (void) notifyOnSessionFailedWithError:(NSError *)error {
    /* A session that completes with an error is considered by interested
     * controllers to be a failure if the error is not `cancelled`. A
     * cancelled session is handled in didBecomeInvalidWithError. */
    if (error != NULL && error.code != SESSION_CANCELLED) {
        NSLog(@"A session failed with client-side error code: %ld, "
              "and description: %@", (long)error.code,
              error.localizedDescription);
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.delegate searchDidNotCompleteWithError:error];
        });
    }
}

/* A check for more results using the last query has completed. Notify any
 * interested controllers if more symbol results were found. */
- (void) notifyOnTotalResults {
    /* The response data should contain a count of JSON objects equal to one or
     * zero. A result of zero means that no further results were found. A result
     * of one means more symbols are available. */
    int more = [self getCountFromResponse:self.responseData];
    if (more == 1) {
        /* Obtain the total number of results. */
        NSString * total = [self getTotalFromResponse:self.responseData];
        /* Hang onto the numeric value in case more results are requested. */
        _totalResults = [total intValue];
        /* Notify the delegate that more results were found. */
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.delegate didGetTotalResultsForLastQuery:total];
        });
    } else if (more > 1) {
        /* Log this. Most likely it means an error in application logic. */
        NSLog(@"When checking for more results got %d", more);
    }
    /* Internal state is set back to processing stock symbols. */
    self.stateIsReadingTotals = NO;
}

/* Obtain a stock entry at an offset into available symbols. The parameter 
 * `index` contains the offset into available symbols requested. Returns the 
 * entry found as a symbol-description pair or nil if it was out-of-bounds. */
- (NSArray *) getStockAtIndex:(long)index {
    if (index < self.symbols.count) {
        return self.symbols[index];
    } else {
        NSLog(@"The stock requested at index %ld is out-of-bounds", index);
    }
    return [NSArray arrayWithObjects: @"", @"", nil];
}

/* Obtain the stock symbol at an offset into available symbols. The parameter
 * `index` contains the offset into available symbols requested. Returns the
 * NSString representation of the stock symbol. */
- (NSString *) getSymbolAtIndex:(long)index {
    return [self getStockAtIndex:index][0]; /* The stock symbol. */
}

/* Obtain the stock description at an offset into available symbols. The
 * parameter `index` contains the offset into available symbols requested.
 * Returns the NSString representation of the stock description. */
- (NSString *) getDescriptionAtIndex:(long)index {
    return [self getStockAtIndex:index][1]; /* The stock description. */
}

/* Request the next window of results using the last query provided. If more
 * results were previously found this will create an NSURLSession containing
 * the query string for the next set of results. The results will then be loaded
 * into responseData for extracting symbols. If no more results were found this
 * method will do nothing. Controllers must have implemented the
 * StockSearchDelegate protocol to be notified of completion. */
- (void) requestNextResults {
    /* Total results is only updated if more results were found. Note the second
     * predicate (unprocessed results) depends on successfully processing the
     * additional results and setting symbols to the updated NSArray. */
    if (!self.moreResultsSessionIsBusy &&         /* Not already busy. */
        self.totalResults > self.symbols.count && /* Unprocessed results. */
        [self nextIndexIsInRange]) {              /* Sanity check on range. */
        self.moreResultsSessionIsBusy = YES; /* Prevent overlapping requests. */
        dispatch_queue_t more = dispatch_queue_create("more", NULL);
        /* Dispatch a more results thread. */
        dispatch_async(more, ^{
            /* Update index for use with getURLForCurrentQueryWithXpath. */
            self.indexIntoResults += YAHOO_MAX_PER_REQUEST;
            NSURL * url = /* Using Xpath for search. */
                [self getURLForCurrentQueryWithXpath:queryUrlXpathSearch];
            [self createSessionWithURL:url];
        });
    } else {
        /* A sanity check failed: 
         * - a more results session may already be active and incomplete.
         * - all results may have already been processed.
         * - the requested index may be out of bounds.*
         * In particular return a notification of no further results, or index
         * out-of-range. These indicate no further results. */
        if (self.totalResults >= self.symbols.count ||
            ![self nextIndexIsInRange]) {
            /* Dispatch a notification thread. */
            dispatch_async(dispatch_get_main_queue(), ^{
                /* A controller should take note that no more results exist. */
                [self.delegate moreResultsDidComplete];
            });
            /* Re-enable more results requests. Although it wouldn't make sense
             * to request more since no more exist.*/
            self.moreResultsSessionIsBusy = NO;
        }
    }
}

/* When a request for more results is made the current index is incremented.
 * However the next index may not be in range. Returns YES if the next index
 * is still in range of available results. Otherwise returns NO. */
- (BOOL) nextIndexIsInRange {
    NSUInteger nextIndex = self.indexIntoResults + YAHOO_MAX_PER_REQUEST;
    if (nextIndex < self.totalResults) {
        /* Supposing the index were incremented. It remains in range. */
        return YES;
    }
    return NO; /* The next index is out-of-range. */
}

/* Restore the internal state for processing response data to defaults. */
- (void) resetInternalState {
    self.stateIsReadingTotals = NO;
    _totalResults = 0; /* Using ivar for public read-only property. */
    self.indexIntoResults = 0;
    self.moreResultsSessionIsBusy = NO;
}

#pragma mark - NSURLSessionDataDelegate

/* NSURLSession delegate for handling the initial server response. */
- (void)URLSession:(NSURLSession *)session
        dataTask:(NSURLSessionDataTask *)dataTask
        didReceiveResponse:(NSURLResponse *)response
        completionHandler:
            (void (^)(NSURLSessionResponseDisposition disposition))
                completionHandler {
    /* Allow all connection requests. */
    completionHandler(NSURLSessionResponseAllow);
    /* Reallocate response data ivar for each new connection. */
    _responseData = [[NSMutableData alloc] init];
}

/* NSURLSession delegate for handling when the application receives data. */
- (void)URLSession:(NSURLSession *)session
        dataTask:(NSURLSessionDataTask *)dataTask
        didReceiveData:(NSData *)data {
    /* Append the new data to the ivar. */
    [self.responseData appendData:data];
}

#pragma mark - NSURLSessionTaskDelegate

/* NSURLSession delegate for handling when a task is completed. Notify any 
 * interested controllers of a failure or process the response data. */
- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task
        didCompleteWithError:(NSError *)error {
    /* Will be NULL if no error (not error code zero) occurred. Cancelled 
     * sessions have an error code of -999. Any other error should be passed to
     * interested controllers. */
    if (error == NULL) {
        if (!self.stateIsReadingTotals) { /* Reading `stock symbols`. */
            /* Notify delegate as required when stock symbols are found. */
            [self notifyOnResults];
        } else { /* Reading `total stock symbols`. */
            /* Notify delegate as required when more symbols are found. */
            [self notifyOnTotalResults];
        }
    } else {
        /* Notify delegate as required when an error occurs. */
        [self notifyOnSessionFailedWithError:error];
    }
}

#pragma mark - NSURLSessionDelegate

/* NSURLSession delegate for handling a connection becoming invalidated. Notify
 * interested controllers if the invalidation was not explicitly requested. A
 * session will be explicitly invalidated to prevent overlapping queries to the
 * server. Most likely this means the query has been changed before the session
 * completed for the last query requested. See also createSessionWithURL. */
- (void)URLSession:(NSURLSession *)session
        didBecomeInvalidWithError:(NSError *)error {
    /* When error is not nil the session has not been explicitly invalidated by
     * sending a cancel message. */
    if (error != nil) {
        NSLog(@"A session was invalidated with client-side error code: %ld, "
              "and description: %@", (long)error.code,
              error.localizedDescription);
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.delegate searchDidNotCompleteWithError:error];
        });
    }
}

@end
