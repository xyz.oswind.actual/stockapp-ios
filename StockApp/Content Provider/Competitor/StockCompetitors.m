//
//  StockCompetitors.m
//  StockApp
//
//  An object that performs stock competitor retrieval and notifies interested
//  controllers when this task is completed using the StockCompetitorsDelegate
//  protocol. The connections are performed asynchronously using NSURLSession.
//  This object is the delegate for all NSURLSession used.
//
//  -- Implementation Notes ----------------------------------------------------
//  This class uses Yahoo! Finance for competitor lookup. The Yahoo URL used is:
//  https://ca.finance.yahoo.com/q?s=QUERY
//  where `s=QUERY` is an existing symbol that may have competitor data.
//
//  The results are pre-filtered and converted to JSON using YQL:
//  https://developer.yahoo.com/yql/ where the select string used is
//
//  select * from html where url='YAHOO_URL_ABOVE' and xpath=
//  '//div[contains(@id,"yfi_comparison")]//div[contains(@class,"bd")]
//  //table/tbody/*'
//
//  Finally, once the JSON object is returned, it is converted to Foundation
//  objects by selecting the appropriate keys or key paths. In general this
//  means less data is read by the iOS device and less local processing is
//  required. Thereby reducing expensive wireless radio use and cpu time.
//  ----------------------------------------------------------------------------
//
//  Created by David McKnight on 2016-03-04.
//  Copyright (C) 2016 David McKnight actual@oswind.xyz
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#import "StockCompetitors.h"

#pragma mark - Private constants

/* Local constants declaring error codes for NSURLSession. */
static const int SESSION_CANCELLED = -999;
/* Local constants declaring the partial strings of a competitors query. */
static const NSString * competitorUrlStart = @"https://query.yahooapis.com/v1/p"
    "ublic/yql?q=select%20*%20from%20html%20where%20url%3D'https%3A%2F%2Fca.fin"
    "ance.yahoo.com%2Fq%3Fs%3D";
static const NSString * competitorUrlEnd = @"'%20and%20xpath%3D'%2F%2Fdiv%5Bcon"
    "tains(%40id%2C%22yfi_comparison%22)%5D%2F%2Fdiv%5Bcontains(%40class%2C%22b"
    "d%22)%5D%2F%2Ftable%2Ftbody%2F*'&format=json&env=store%3A%2F%2Fdatatables."
    "org%2Falltableswithkeys";

#pragma mark - Private declaration

@interface StockCompetitors()

/* Contains the query response data obtained from the server. */
@property (nonatomic, strong) NSMutableData * responseData;

/* Contains the current query session. May not be running or valid. */
@property (nonatomic, strong) NSURLSession * statusSession;

@property (nonatomic) NSUInteger competitorsCount;

/* A check for competitors has failed due to a client-side error. Notify any
 * interested controllers of the failure and provide the NSError object. */
- (void) notifyOnSessionFailedWithError:(NSError *)error;

/* A check for stock competitors has completed. Validate and/or parse response 
 * data and notify any interested controllers of the data being available. */
- (void) notifyOnCompetitorsAvailable;

/* Get an URL corresponding to a competitors query. The parameter `symbol`
 * should contain a valid stock symbol. No checks for validity of the symbol
 * are performed. */
- (NSURL *) getURLForSymbol:(NSString *)symbol;

/* Create a NSURLSession using the NSURL passed. Any existing sessions will be
 * cancelled to prevent multiple queries being in an incomplete state. */
- (void) createSessionWithURL:(NSURL *)url;

/* The count contained in a JSON formatted result is the authoritative count of
 * useable objects described. */
- (int) getCountFromResponse:(NSData *)data;

/* Parse the competitors data from the response data. */
- (void) extractCompetitorsFromResponse:(NSData *)data;

@end

#pragma mark - Public implementation

@implementation StockCompetitors

/* Parse the competitors data from the response data. */
- (void) extractCompetitorsFromResponse:(NSData *)data {
    /* Convert the response data to a Foundation object. */
    NSArray * results = [[NSJSONSerialization JSONObjectWithData:data
                              options:NSJSONReadingMutableContainers error:NULL]
                              valueForKeyPath:@"query.results.tr"];
    /* Store the extracted competitors (if any) in a temporary location. */
    NSMutableArray * extracted = [[NSMutableArray alloc] init];
    /* Enumerate over competitor data. Skip the first item which contains the 
     * currently selected stock. */
    for (int i = 1; i < self.competitorsCount; i++) {
        NSString * symbol; /* Forward declare the symbol string. */
        /* As of April 2016 Yahoo! can sometimes provide malformed data in 
         * competitor response data. Specifically Yahoo! doesn't appear to 
         * consistently update the trading symbol when it changes. An example 
         * would be the merge of Burger King and Tim Hortons where trading of 
         * previous symbols ceased and a new symbol, QSR, was introduced (end of
         * 2014). Previous references should have been updated, and this didn't 
         * happen. As a result the JSON response becomes slightly different when
         * it contains these symbols. This difference triggers an exception.
         * Here the implementation checks for this Yahoo! bug using try/catch.
         * REFERENCE: Symbol GTIM and MCD include BKW instead of QSR. */
        @try {
            /* Extract the competitor's symbol. This will throw an exception 
             * when encountering the Yahoo! bug described above. */
            symbol = [results[i] valueForKeyPath:@"td.a.content"][0];
        }
        @catch (NSException * exception) {
            NSLog(@"Encountered a malformed competitor during "
                   "extractCompetitorsFromResponse. Skipping entry.");
            continue; /* Skip the entry. */
        }
        /* Extract the competitor's company name. */
        NSString * name = [results[i] valueForKeyPath:@"td.a.title"][0];
        /* Extract the nested entry containing change, and market cap. */
        NSArray * nestedEntry = [results[i] valueForKeyPath:@"td.span"];
        /* Guard against missing change data from content provider. */
        NSString * change = [[nestedEntry[1] valueForKeyPath:@"b.content"]
                isKindOfClass:[NSString class]]? /* Check if a string exists. */
                    [nestedEntry[1] valueForKeyPath:@"b.content"]: @"N/A";
        /* Guard against missing up/down indicator from content provider. */
        NSString * upDown = [[nestedEntry[1] valueForKeyPath:@"img.alt"]
                isKindOfClass:[NSString class]]? /* Check if a string exists. */
                    [nestedEntry[1] valueForKeyPath:@"img.alt"]: @"";
        /* Replace the up/down string with a single +/- character. Check for 
         * each possibility explicitly since the content provider may not 
         * provide this data for every request. This ensures that if the data is
         * missing it will be saved as `N/A`. */
        if ([upDown isEqualToString:@"Up"]) {
            upDown = @"+"; /* Replace `Up` with `+`. */
        } else if ([upDown isEqualToString:@"Down"]) {
            upDown = @"-"; /* Replace `Down` with `-`. */
        }
        /* Prepare the final change string for use in views. */
        change = [NSString stringWithFormat:@"%@%@", upDown, change];
        /* Guard against missing market cap data from content provider. */
        NSString * mktCap = [[nestedEntry[2] valueForKeyPath:@"content"]
                isKindOfClass:[NSString class]]? /* Check if a string exists. */
                    [nestedEntry[2] valueForKeyPath:@"content"]: @"N/A";
        /* Prepare the competitor entry. */
        NSArray * competitor =
                [NSArray arrayWithObjects:symbol,name,change,mktCap,nil];
        /* Add the competitor entry to the list of competitors. */
        [extracted addObject:competitor];
    }
    /* Make the competitors data available via competitorsData. */
    _competitorData = extracted;
}

/* Convenience method which accepts one parameter `symbol`. The symbol provided
 * must exist otherwise this method will not return any useful information. The
 * symbol provided is used to obtain the competitors of the stock. Once the
 * competitors data is parsed it becomes available via the public property
 * defined above. */
- (void) getCompetitorsWithSymbol:(NSString *)symbol {
    /* Guard against a competitors query with empty symbols. */
    if (![symbol isEqualToString:@""] && symbol != nil) {
        dispatch_queue_t competitor = dispatch_queue_create("competitor", NULL);
        /* Dispatch a status thread. */
        dispatch_async(competitor, ^{
            /* Create the NSURL using the provided symbol. */
            NSURL * url = [self getURLForSymbol:symbol];
            /* Request current status data. */
            [self createSessionWithURL:url];
        });
    }
}

/* Get an URL corresponding to a competitors query. The parameter `symbol`
 * should contain a valid stock symbol. No checks for validity of the symbol
 * are performed. */
- (NSURL *) getURLForSymbol:(NSString *)symbol {
    /* The string representation of the URL. */
    NSString * urlString = [NSString stringWithFormat:@"%@%@%@",
                            competitorUrlStart, symbol, competitorUrlEnd];
    return [NSURL URLWithString:urlString];
}

/* Create a NSURLSession using the NSURL passed. Any existing sessions will be
 * cancelled to prevent multiple queries being in an incomplete state. */
- (void) createSessionWithURL:(NSURL *)url {
    /* Cancel any existing sessions that have not yet completed. */
    [self.statusSession invalidateAndCancel];
    /* Create a new aysynchronous session with default caching and cookies. */
    _statusSession = [NSURLSession sessionWithConfiguration:
                      [NSURLSessionConfiguration defaultSessionConfiguration]
                        delegate:self delegateQueue:nil];
    /* Create the HTTPS GET request with the specified URL. */
    NSURLSessionDataTask * task = [self.statusSession dataTaskWithURL:url];
    /* Perform the HTTPS GET request. */
    [task resume];
}

/* A check for stock competitors has completed. Validate and/or parse response
 * data and notify any interested controllers of the data being available. */
- (void) notifyOnCompetitorsAvailable {
    /* Check if competitors were returned in the response data. */
    self.competitorsCount = [self getCountFromResponse:self.responseData];
    if (self.competitorsCount > 0) {
        /* Extract the competitors data from the response data. */
        [self extractCompetitorsFromResponse:self.responseData];
    }
    /* Notify interested controllers that the competitor data is available. */
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.delegate competitorsRetrieved];
    });
}

/* A check for competitors has failed due to a client-side error. Notify any
 * interested controllers of the failure and provide the NSError object. */
- (void) notifyOnSessionFailedWithError:(NSError *)error {
    /* A session that completes with an error is considered by interested
     * controllers to be a failure if the error is not `cancelled`. A
     * cancelled session is handled in didBecomeInvalidWithError. */
    if (error != NULL && error.code != SESSION_CANCELLED) {
        NSLog(@"A session failed with client-side error code: %ld, "
              "and description: %@", (long)error.code,
              error.localizedDescription);
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.delegate competitorsNotRetrievedWithError:error];
        });
    }
}

/* The count contained in a JSON formatted result is the authoritative count of
 * useable objects described. */
- (int) getCountFromResponse:(NSData *)data {
    return [[[NSJSONSerialization JSONObjectWithData:data
                    options:NSJSONReadingMutableLeaves error:NULL]
                    valueForKeyPath:@"query.count"] intValue];
}

#pragma mark - NSURLSessionDataDelegate

/* NSURLSession delegate for handling the initial server response. */
- (void)URLSession:(NSURLSession *)session
        dataTask:(NSURLSessionDataTask *)dataTask
        didReceiveResponse:(NSURLResponse *)response
        completionHandler:
            (void (^)(NSURLSessionResponseDisposition disposition))
                completionHandler {
    /* Allow all connection requests. */
    completionHandler(NSURLSessionResponseAllow);
    /* Reallocate response data ivar for each new connection. */
    _responseData = [[NSMutableData alloc] init];
}

/* NSURLSession delegate for handling when the application receives data. */
- (void)URLSession:(NSURLSession *)session
        dataTask:(NSURLSessionDataTask *)dataTask
        didReceiveData:(NSData *)data {
    /* Append the new data to the ivar. */
    [self.responseData appendData:data];
}

#pragma mark - NSURLSessionTaskDelegate

/* NSURLSession delegate for handling when a task is completed. Notify any
 * interested controllers of a failure or process the response data. */
- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task
        didCompleteWithError:(NSError *)error {
    /* Will be NULL if no error (not error code zero) occurred. Cancelled
     * sessions have an error code of -999. Any other error should be passed to
     * interested controllers. */
    if (error == NULL) {
        /* Notify delegate of the available status. */
        [self notifyOnCompetitorsAvailable];
    } else {
        /* Notify delegate as required when an error occurs. */
        [self notifyOnSessionFailedWithError:error];
    }
}

#pragma mark - NSURLSessionDelegate

/* NSURLSession delegate for handling a connection becoming invalidated. Notify
 * interested controllers if the invalidation was not explicitly requested. A
 * session will be explicitly invalidated to prevent overlapping queries to the
 * server. Most likely this means the query has been changed before the session
 * completed for the last query requested. See also createSessionWithURL. */
- (void)URLSession:(NSURLSession *)session
        didBecomeInvalidWithError:(NSError *)error {
    /* When error is not nil the session has not been explicitly invalidated by
     * sending a cancel message. */
    if (error != nil) {
        NSLog(@"A session was invalidated with client-side error code: %ld, "
              "and description: %@", (long)error.code,
              error.localizedDescription);
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.delegate competitorsNotRetrievedWithError:error];
        });
    }
}

@end
