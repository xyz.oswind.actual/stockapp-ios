//
//  StockCompetitors.h
//  StockApp
//
//  An object that performs stock competitor retrieval and notifies interested
//  controllers when this task is completed using the StockCompetitorsDelegate
//  protocol. The connections are performed asynchronously using NSURLSession.
//  This object is the delegate for all NSURLSession used.
//
//  -- Implementation Notes ----------------------------------------------------
//  This class uses Yahoo! Finance for competitor lookup. The Yahoo URL used is:
//  https://ca.finance.yahoo.com/q?s=QUERY
//  where `s=QUERY` is an existing symbol that may have competitor data.
//
//  The results are pre-filtered and converted to JSON using YQL:
//  https://developer.yahoo.com/yql/ where the select string used is
//
//  select * from html where url='YAHOO_URL_ABOVE' and xpath=
//  '//div[contains(@id,"yfi_comparison")]//div[contains(@class,"bd")]
//  //table/tbody/*'
//
//  Finally, once the JSON object is returned, it is converted to Foundation
//  objects by selecting the appropriate keys or key paths. In general this
//  means less data is read by the iOS device and less local processing is
//  required. Thereby reducing expensive wireless radio use and cpu time.
//  ----------------------------------------------------------------------------
//
//  Created by David McKnight on 2016-03-04.
//  Copyright (C) 2016 David McKnight actual@oswind.xyz
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#import <Foundation/Foundation.h>
#import "StockCompetitorsDelegate.h"

#pragma mark - Public declaration

@interface StockCompetitors : NSObject
<NSURLSessionDelegate, NSURLSessionTaskDelegate, NSURLSessionDataDelegate>

/* Notify a controller that implements the StockCompetitorsDelegate protocol 
 * when stock competitor queries complete or when errors occur. */
@property (nonatomic,strong) id <StockCompetitorsDelegate> delegate;

/* The parsed competitor data for use with table views. */
@property (nonatomic,strong,readonly) NSArray * competitorData;

/* Convenience method which accepts one parameter `symbol`. The symbol provided
 * must exist otherwise this method will not return any useful information. The
 * symbol provided is used to obtain the competitors of the stock. Once the
 * competitors data is parsed it becomes available via the public property
 * defined above. */
- (void) getCompetitorsWithSymbol:(NSString *)symbol;

@end
