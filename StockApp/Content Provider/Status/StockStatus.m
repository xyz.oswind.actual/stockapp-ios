//
//  StockStatus.m
//  StockApp
//
//  An object that performs symbol status checks and notifies interested
//  controllers when this task is completed using the StockStatusDelegate
//  protocol. The connections are performed asynchronously using NSURLSession.
//  This object is the delegate for all NSURLSession used.
//
//  -- Implementation Notes ----------------------------------------------------
//  This class uses Yahoo! Finance's API for obtaining current stock status. The
//  Yahoo URL used is:
//  ""download.finance.yahoo.com/d/quotes.csv?
//                                  s=QUERY&f=snc4c1p2pobat8mwva2j1d1t1&e=.csv""
//  where `s=QUERY` is the symbol status requested, and the string following
//  `&f=` represents API tags for the columns of interest. These tags are found
//  from many sources online, but not documented in any official way (so are
//  subject to change).
//
//  The results are converted to JSON using YQL:
//  https://developer.yahoo.com/yql/ . Where the select string used is:
//      select * from csv where url='YAHOO_URL_ABOVE' and
//  columns='symbol,desc,currency,change,percent_change,prev_close,open,bid,ask,
//  year_target_est,days_range,year_range,volume,avg_volume,market_cap,
//  last_trade_date,last_trade_time'
//
//  For reference all columns can be retrieved using the select string:
//      select * from yahoo.finance.quotes where symbol='QUERY'
//
//  Finally, once the JSON object is returned, it is converted to Foundation
//  objects by selecting the appropriate keys or key paths. In general this
//  means less data is read by the iOS device and less local processing is
//  required. Thereby reducing expensive wireless radio use and cpu time.
//  ----------------------------------------------------------------------------
//
//  Created by David McKnight on 2016-03-01.
//  Copyright (C) 2016 David McKnight actual@oswind.xyz
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#import "StockStatus.h"
#import "StockChart.h"
#import "StockCompetitors.h"

#pragma mark - Private constants

/* Local constants declaring error codes for NSURLSession. */
static const int SESSION_CANCELLED = -999;
/* Local constants declaring the partial strings of a stock status query. */
static const NSString * statusUrlStart = @"https://query.yahooapis.com/v1/publi"
    "c/yql?q=select%20*%20from%20csv%20where%20url%3D'http%3A%2F%2Fdownload.fin"
    "ance.yahoo.com%2Fd%2Fquotes.csv%3Fs%3D";
static const NSString * statusUrlEnding = @"%26f%3Dsnc4c1p2pobat8mwva2j1d1t1%26"
    "e%3D.csv'%20and%20columns%3D'symbol%2Cdesc%2Ccurrency%2Cchange%2Cpercent_c"
    "hange%2Cprev_close%2Copen%2Cbid%2Cask%2Cyear_target_est%2Cdays_range%2Cyea"
    "r_range%2Cvolume%2Cavg_volume%2Cmarket_cap%2Clast_trade_date%2Clast_trade_"
    "time'&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";

#pragma mark - Private declaration

@interface StockStatus()

/* Hang onto the last provided symbol in case something goes wrong. */
@property (nonatomic, strong) NSString * lookup;

/* Contains the query response data obtained from the server. */
@property (nonatomic, strong) NSMutableData * responseData;

/* Contains the current query session. May not be running or valid. */
@property (nonatomic, strong) NSURLSession * statusSession;

/* Internal state determining if data is still being processed. */
@property (atomic) BOOL statusDone;
@property (atomic) BOOL chartDone;
@property (atomic) BOOL competitorsDone;

/* Contains the chart data for the stock status. */
@property (nonatomic,strong) StockChart * stockChart;

/* Contains the competitors summary data for the stock status. */
@property (nonatomic,strong) StockCompetitors * stockCompetitors;

/* Get an URL corresponding to a current status lookup. The parameter `symbol`
 * should contain a valid stock symbol. No checks for validity of the symbol
 * are performed. */
- (NSURL *) getURLForSymbol:(NSString *)symbol;

/* Create a NSURLSession using the NSURL passed. Any existing sessions will be
 * cancelled to prevent multiple queries being in an incomplete state. */
- (void) createSessionWithURL:(NSURL *)url;

/* A check for stock status has failed due to a client-side error. Notify any
 * interested controllers of the failure and provide the NSError object. */
- (void) notifyOnSessionFailedWithError:(NSError *)error;

/* The count contained in a JSON formatted result is the authoritative count of
 * useable objects described. */
- (int) getCountFromResponse:(NSData *)data;

/* A check for stock status has completed. Validate response data and notify any
 * interested controllers of the status being available.*/
- (void) notifyOnStatusAvailable;

/* Extract the stock status from the query response data and make it available
 * in the public properties. The NSData parameter `data` contains the unfiltered
 * response from a completed status query session. */
- (void) getStatusFromResponse:(NSData *)data;

/* Check if the response data contains a single result. This corresponds to a 
 * valid stock status for parsing. Returns YES if the response data contains a
 * single result. Otherwise returns NO. */
- (BOOL) responseContainsValidResult;

/* Check if the internal state signals the processing of data is complete. 
 * Validate response data and notify any interested controllers the Status and
 * Chart requests are completed. */
- (void) notifyOnReadyToPostStatus;

/* Reset this objects internal state to initial values. */
- (void) resetInternalState;

@end

#pragma mark - Public implementation

@implementation StockStatus

/* Convenience method which accepts one parameter `symbol`. The symbol provided
 * must exist otherwise this method will not return any useful information. The
 * symbol provided is used to obtain the current status of the stock. Once the
 * current status is parsed the stock status becomes available via the public
 * properties defined above. This method *may* be called multiple times with
 * different symbols to serially obtain multiple current statuses. Note that if
 * this is desired it would be best to make copies of the NSString properties.
 * Notification of available status data will be provided by implementing the
 * StockStatusDelegate protocol. */
- (void) getStatusWithSymbol:(NSString *)symbol {
    /* Guard against status lookup with empty symbols. */
    if (![symbol isEqualToString:@""] && symbol != nil) {
        /* Reset to initial state. */
        [self resetInternalState];
        /* Dispatch a status thread. */
        dispatch_queue_t status = dispatch_queue_create("status", NULL);
        dispatch_async(status, ^{
            /* Hang onto the symbol in case something goes wrong. */
            self.lookup = symbol;
            /* Create the NSURL using the provided symbol. */
            NSURL * url = [self getURLForSymbol:symbol];
            /* Request current status data. */
            [self createSessionWithURL:url];
            /* Asynchronously request chart data. */
            [self.stockChart getChartWithSymbol:symbol];
            /* Asynchronously request competitors data. */
            [self.stockCompetitors getCompetitorsWithSymbol:symbol];
        });
    }
}

/* Reset this objects internal state to initial values. */
- (void) resetInternalState {
    self.statusDone = NO;
    self.chartDone = NO;
    self.competitorsDone = NO;
}

/* Contains the chart data for the stock status. */
- (StockChart *) stockChart {
    if (_stockChart == nil) {
        _stockChart = [[StockChart alloc] init];
        _stockChart.delegate = self;
    }
    return _stockChart;
}

/* Contains the competitors summary data for the stock status. */
- (StockCompetitors *) stockCompetitors {
    if (_stockCompetitors == nil) {
        _stockCompetitors = [[StockCompetitors alloc] init];
        _stockCompetitors.delegate = self;
    }
    return _stockCompetitors;
}

/* Check if the internal state signals the processing of data is complete.
 * Validate response data and notify any interested controllers the Status and
 * Chart requests are completed. */
- (void) notifyOnReadyToPostStatus {
    if (self.statusDone && self.chartDone && self.competitorsDone) {
        /* Notify interested controllers that the status is available. */
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.delegate statusDidComplete];
        });
        [self resetInternalState];
    }
}

/* Get an URL corresponding to a current status lookup. The parameter `symbol`
 * should contain a valid stock symbol. No checks for validity of the symbol
 * are performed. */
- (NSURL *) getURLForSymbol:(NSString *)symbol {
    /* The string representation of the URL. */
    NSString * urlString = [NSString stringWithFormat:@"%@%@%@",
                            statusUrlStart, symbol, statusUrlEnding];
    return [NSURL URLWithString:urlString];
}

/* Create a NSURLSession using the NSURL passed. Any existing sessions will be
 * cancelled to prevent multiple queries being in an incomplete state. */
- (void) createSessionWithURL:(NSURL *)url {
    /* Cancel any existing sessions that have not yet completed. */
    [self.statusSession invalidateAndCancel];
    /* Create a new aysynchronous session with default caching and cookies. */
    _statusSession = [NSURLSession sessionWithConfiguration:
                      [NSURLSessionConfiguration defaultSessionConfiguration]
                        delegate:self delegateQueue:nil];
    /* Create the HTTPS GET request with the specified URL. */
    NSURLSessionDataTask * task = [self.statusSession dataTaskWithURL:url];
    /* Perform the HTTPS GET request. */
    [task resume];
}

/* A check for stock status has failed due to a client-side error. Notify any
 * interested controllers of the failure and provide the NSError object. */
- (void) notifyOnSessionFailedWithError:(NSError *)error {
    /* A session that completes with an error is considered by interested
     * controllers to be a failure if the error is not `cancelled`. A
     * cancelled session is handled in didBecomeInvalidWithError. */
    if (error != NULL && error.code != SESSION_CANCELLED) {
        NSLog(@"A session failed with client-side error code: %ld, "
              "and description: %@", (long)error.code,
              error.localizedDescription);
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.delegate statusDidNotCompleteWithError:error];
        });
    }
}

/* The count contained in a JSON formatted result is the authoritative count of
 * useable objects described. */
- (int) getCountFromResponse:(NSData *)data {
    return [[[NSJSONSerialization JSONObjectWithData:data
                    options:NSJSONReadingMutableLeaves error:NULL]
                        valueForKeyPath:@"query.count"] intValue];
}

/* A check for stock status has completed. Validate response data and notify any
 * interested controllers of the status being available.*/
- (void) notifyOnStatusAvailable {
    /* Check if the response contains a valid result. */
    if ([self responseContainsValidResult]) {
        /* Extract the status into public properties. */
        [self getStatusFromResponse:self.responseData];
        /* Notify interested controllers that the status is available. */
        dispatch_async(dispatch_get_main_queue(), ^{
            self.statusDone = YES;
            [self notifyOnReadyToPostStatus];
        });
    }
}

/* Check if the response data contains a single result. This corresponds to a
 * valid stock status for parsing. Returns YES if the response data contains a
 * single result. Otherwise returns NO. */
- (BOOL) responseContainsValidResult {
    /* Check if the response contains a valid result. */
    int count = [self getCountFromResponse:self.responseData];
    if (count != 1) {
        /* Something is wrong. Response data doesn't contain a single result. */
        NSString * msg = [NSString stringWithFormat:
            @"Response data for symbol `%@` contained %d results.",
            self.lookup, count];
        NSLog(@"%@",msg);
        /* Notify any interested controllers of the failure. */
        NSError * error = [NSError errorWithDomain:msg code:0 userInfo:nil];
        [self notifyOnSessionFailedWithError:error];
        /* A single result was not found. Response data is invalid. */
        return NO;
    }
    /* A single result was found. Indicating a valid status in response data. */
    return YES;
}

/* Extract the stock status from the query response data and make it available
 * in the public properties. The NSData parameter `data` contains the unfiltered
 * response from a completed status query session. */
- (void) getStatusFromResponse:(NSData *)data {
    /* Convert the response data to a Foundation object. */
    NSDictionary * status = [[NSJSONSerialization JSONObjectWithData:data
        options:NSJSONReadingMutableContainers error:NULL]
        valueForKeyPath:@"query.results.row"];
    /* Setup the public properties. Using copies of string data to allow the
     * `status` object to be deallocated by ARC. */
    _symbol = [NSString stringWithString:[status valueForKey:@"symbol"]];
    _desc = [NSString stringWithString:[status valueForKey:@"desc"]];
    _currency = [NSString stringWithString:[status valueForKey:@"currency"]];
    _change = [NSString stringWithString:[status valueForKey:@"change"]];
    _percentChange = [NSString stringWithString:
                      [status valueForKey:@"percent_change"]];
    _prevClose = [NSString stringWithString:[status valueForKey:@"prev_close"]];
    _open = [NSString stringWithString:[status valueForKey:@"open"]];
    _bid = [NSString stringWithString:[status valueForKey:@"bid"]];
    _ask = [NSString stringWithString:[status valueForKey:@"ask"]];
    _yearTargetEst = [NSString stringWithString:
                      [status valueForKey:@"year_target_est"]];
    _daysRange = [NSString stringWithString:[status valueForKey:@"days_range"]];
    _yearsRange = [NSString stringWithString:
                   [status valueForKey:@"year_range"]];
    _volume = [NSString stringWithString:[status valueForKey:@"volume"]];
    _avgVolume = [NSString stringWithString:[status valueForKey:@"avg_volume"]];
    _marketCap = [NSString stringWithString:[status valueForKey:@"market_cap"]];
    /* Reformat last trade date to NSDateFormatterMediumStyle. */
    NSDateFormatter * dateFormat = [[NSDateFormatter alloc] init];
    dateFormat.dateFormat = @"MM/dd/yyyy";
    /* Get the last trade date in it's original format. */
    NSDate * srcDate = [dateFormat dateFromString:
                        [status valueForKey:@"last_trade_date"]];
    /* Now re-format last trade date to desired locale style. */
    dateFormat.dateStyle = NSDateFormatterMediumStyle;
    dateFormat.timeStyle = NSDateFormatterNoStyle;
    _lastTradeDate = [dateFormat stringFromDate:srcDate];
    /* Reformat last trade time to include `EST`. */
    _lastTradeTime = [NSString stringWithFormat:@"%@ EST",
                      [status valueForKey:@"last_trade_time"]];
    /* Make sure ARC optimistically deallocates internals. */
    status = nil;
    self.responseData = nil;
}

#pragma mark - StockChartDelegate

/* A chart query which was previously submitted has now completed. Controllers
 * can now update their views with the StockChart. */
- (void) chartDidRetrieve {
    /* Update internal state to indicate the chart data is ready. */
    self.chartDone = YES;
    /* Make the chart data available. */
    _chartData = self.stockChart.chartData;
    [self notifyOnReadyToPostStatus];
}

/* A chart query which was previously submitted did not complete. The parameter
 * `error` contains the cause of the failure. */
- (void) chartDidNotRetrieveWithError:(NSError *)error {
    /* Nothing to do on an error except ignore it. */
    self.chartDone = YES;
    [self notifyOnReadyToPostStatus];
}

#pragma mark - StockCompetitorsDelegate

/* A competitor query which was previously submitted has now completed.
 * Controllers can now update their views with the data. */
- (void) competitorsRetrieved {
    /* Update internal state to indicate the competitor data is ready. */
    self.competitorsDone = YES;
    /* Make competitor data available. */
    _comparisonData = self.stockCompetitors.competitorData;
    [self notifyOnReadyToPostStatus];
}

/* A competitor query which was previously submitted did not complete. The
 * parameter `error` contains the cause of the failure. */
- (void) competitorsNotRetrievedWithError:(NSError *)error {
    /* Not doing anything else if an error occurred. */
    self.competitorsDone = YES;
    [self notifyOnReadyToPostStatus];
}

#pragma mark - NSURLSessionDataDelegate

/* NSURLSession delegate for handling the initial server response. */
- (void)URLSession:(NSURLSession *)session
        dataTask:(NSURLSessionDataTask *)dataTask
        didReceiveResponse:(NSURLResponse *)response
        completionHandler:
            (void (^)(NSURLSessionResponseDisposition disposition))
                completionHandler {
    /* Allow all connection requests. */
    completionHandler(NSURLSessionResponseAllow);
    /* Reallocate response data ivar for each new connection. */
    _responseData = [[NSMutableData alloc] init];
}

/* NSURLSession delegate for handling when the application receives data. */
- (void)URLSession:(NSURLSession *)session
        dataTask:(NSURLSessionDataTask *)dataTask
        didReceiveData:(NSData *)data {
    /* Append the new data to the ivar. */
    [self.responseData appendData:data];
}

#pragma mark - NSURLSessionTaskDelegate

/* NSURLSession delegate for handling when a task is completed. Notify any
 * interested controllers of a failure or process the response data. */
- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task
        didCompleteWithError:(NSError *)error {
    /* Will be NULL if no error (not error code zero) occurred. Cancelled
     * sessions have an error code of -999. Any other error should be passed to
     * interested controllers. */
    if (error == NULL) {
        /* Notify delegate of the available status. */
        [self notifyOnStatusAvailable];
    } else {
        /* Notify delegate as required when an error occurs. */
        [self notifyOnSessionFailedWithError:error];
    }
}

#pragma mark - NSURLSessionDelegate

/* NSURLSession delegate for handling a connection becoming invalidated. Notify
 * interested controllers if the invalidation was not explicitly requested. A
 * session will be explicitly invalidated to prevent overlapping queries to the
 * server. Most likely this means the query has been changed before the session
 * completed for the last query requested. See also createSessionWithURL. */
- (void)URLSession:(NSURLSession *)session
        didBecomeInvalidWithError:(NSError *)error {
    /* When error is not nil the session has not been explicitly invalidated by
     * sending a cancel message. */
    if (error != nil) {
        NSLog(@"A session was invalidated with client-side error code: %ld, "
              "and description: %@", (long)error.code,
              error.localizedDescription);
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.delegate statusDidNotCompleteWithError:error];
        });
    }
}

@end