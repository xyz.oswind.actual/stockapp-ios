//
//  StockStatus.h
//  StockApp
//
//  An object that performs symbol status checks and notifies interested
//  controllers when this task is completed using the StockStatusDelegate
//  protocol. The connections are performed asynchronously using NSURLSession.
//  This object is the delegate for all NSURLSession used.
//
//  -- Implementation Notes ----------------------------------------------------
//  This class uses Yahoo! Finance's API for obtaining current stock status. The
//  Yahoo URL used is:
//  ""download.finance.yahoo.com/d/quotes.csv?
//                                  s=QUERY&f=snc4c1p2pobat8mwva2j1d1t1&e=.csv""
//  where `s=QUERY` is the symbol status requested, and the string following
//  `&f=` represents API tags for the columns of interest. These tags are found
//  from many sources online, but not documented in any official way (so are
//  subject to change).
//
//  The results are converted to JSON using YQL:
//  https://developer.yahoo.com/yql/ . Where the select string used is:
//      select * from csv where url='YAHOO_URL_ABOVE' and
//  columns='symbol,desc,currency,change,percent_change,prev_close,open,bid,ask,
//  year_target_est,days_range,year_range,volume,avg_volume,market_cap,
//  last_trade_date,last_trade_time'
//
//  For reference all columns can be retrieved using the select string:
//      select * from yahoo.finance.quotes where symbol='QUERY'
//
//  Finally, once the JSON object is returned, it is converted to Foundation
//  objects by selecting the appropriate keys or key paths. In general this
//  means less data is read by the iOS device and less local processing is
//  required. Thereby reducing expensive wireless radio use and cpu time.
//  ----------------------------------------------------------------------------
//
//  Created by David McKnight on 2016-03-01.
//  Copyright (C) 2016 David McKnight actual@oswind.xyz
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#import <Foundation/Foundation.h>
#import "StockStatusDelegate.h"
#import "StockChartDelegate.h"
#import "StockCompetitorsDelegate.h"

#pragma mark - Public declaration

@interface StockStatus : NSObject
<NSURLSessionDelegate, NSURLSessionTaskDelegate, NSURLSessionDataDelegate,
StockChartDelegate, StockCompetitorsDelegate>

/* Notify a controller that implements the StockStatusDelegate protocol when
 * stock status queries complete or when errors occur. */
@property (nonatomic,strong) id <StockStatusDelegate> delegate;

/* Properties which will contain the current information for a Stock. This data
 * is sourced using the Yahoo! Finance API for symbol status. Only once the data
 * is parsed out of the raw response data these properties become available. 
 * Generally this means waiting until a notification is received by implementing
 * the StockStatusDelegate protocol. */
@property (nonatomic,strong,readonly) NSString * symbol;
@property (nonatomic,strong,readonly) NSString * desc;
@property (nonatomic,strong,readonly) NSString * currency;
@property (nonatomic,strong,readonly) NSString * change;
@property (nonatomic,strong,readonly) NSString * percentChange;
@property (nonatomic,strong,readonly) NSString * prevClose;
@property (nonatomic,strong,readonly) NSString * open;
@property (nonatomic,strong,readonly) NSString * bid;
@property (nonatomic,strong,readonly) NSString * ask;
@property (nonatomic,strong,readonly) NSString * yearTargetEst;
@property (nonatomic,strong,readonly) NSString * daysRange;
@property (nonatomic,strong,readonly) NSString * yearsRange;
@property (nonatomic,strong,readonly) NSString * volume;
@property (nonatomic,strong,readonly) NSString * avgVolume;
@property (nonatomic,strong,readonly) NSString * marketCap;
@property (nonatomic,strong,readonly) NSString * lastTradeDate;
@property (nonatomic,strong,readonly) NSString * lastTradeTime;

/* Contains the chart data retrieved by StockChart. */
@property (nonatomic,strong,readonly) NSData * chartData;

/* Contains the stock competitor and comparison data. */
@property (nonatomic,strong,readonly) NSArray * comparisonData;

/* Convenience method which accepts one parameter `symbol`. The symbol provided
 * must exist otherwise this method will not return any useful information. The
 * symbol provided is used to obtain the current status of the stock. Once the 
 * current status is parsed the stock status becomes available via the public
 * properties defined above. This method *may* be called multiple times with
 * different symbols to serially obtain multiple current statuses. Note that if
 * this is desired it would be best to make copies of the NSString properties.
 * Notification of available status data will be provided by implementing the
 * StockStatusDelegate protocol. */
- (void) getStatusWithSymbol:(NSString *)symbol;

@end
