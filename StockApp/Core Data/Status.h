//
//  Status.h
//  StockApp
//
//  Created by David McKnight on 3/18/16.
//  Copyright (C) 2016 David McKnight actual@oswind.xyz
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Chart, Competitor, Stock;

@interface Status : NSManagedObject

@property (nonatomic, retain) NSString * ask;
@property (nonatomic, retain) NSString * avgVolume;
@property (nonatomic, retain) NSString * bid;
@property (nonatomic, retain) NSString * change;
@property (nonatomic, retain) NSString * currency;
@property (nonatomic, retain) NSString * dayRange;
@property (nonatomic, retain) NSString * lastTradeDate;
@property (nonatomic, retain) NSString * lastTradeTime;
@property (nonatomic, retain) NSString * marketCap;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * open;
@property (nonatomic, retain) NSString * percentChange;
@property (nonatomic, retain) NSString * prevClose;
@property (nonatomic, retain) NSString * symbol;
@property (nonatomic, retain) NSString * volume;
@property (nonatomic, retain) NSString * yearEst;
@property (nonatomic, retain) NSString * yearRange;
@property (nonatomic, retain) Stock * watchedStock;
@property (nonatomic, retain) NSSet * statusCompetitors;
@property (nonatomic, retain) Chart * statusChart;
@end

@interface Status (CoreDataGeneratedAccessors)

- (void)addStatusCompetitorsObject:(Competitor *)value;
- (void)removeStatusCompetitorsObject:(Competitor *)value;
- (void)addStatusCompetitors:(NSSet *)values;
- (void)removeStatusCompetitors:(NSSet *)values;

@end
