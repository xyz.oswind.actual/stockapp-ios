//
//  Competitor.m
//  StockApp
//
//  Created by David McKnight on 3/18/16.
//  Copyright (C) 2016 David McKnight actual@oswind.xyz
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#import "Competitor.h"
#import "Status.h"

@implementation Competitor

@dynamic change;
@dynamic marketCap;
@dynamic name;
@dynamic symbol;
@dynamic competitorStatus;

@end
