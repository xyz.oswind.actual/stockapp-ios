//
//  Status.m
//  StockApp
//
//  Created by David McKnight on 3/18/16.
//  Copyright (C) 2016 David McKnight actual@oswind.xyz
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#import "Status.h"
#import "Competitor.h"
#import "Stock.h"

@implementation Status

@dynamic ask;
@dynamic avgVolume;
@dynamic bid;
@dynamic change;
@dynamic currency;
@dynamic dayRange;
@dynamic lastTradeDate;
@dynamic lastTradeTime;
@dynamic marketCap;
@dynamic name;
@dynamic open;
@dynamic percentChange;
@dynamic prevClose;
@dynamic symbol;
@dynamic volume;
@dynamic yearEst;
@dynamic yearRange;
@dynamic watchedStock;
@dynamic statusCompetitors;
@dynamic statusChart;

@end
