//
//  StockWatchViewController.h
//  StockApp
//
//  The controller for a view responsible for displaying watched stocks. These
//  stocks are persisted in Core Data. Responds to touch events on row entries
//  which will load the stock detail view. This controller also acts as a
//  delegate to StockSearchView implementing StockSearchViewDelegate. This
//  allows the controller to update Core Data when a stock is selected for
//  watching from StockSearchView. The table view is updated using a
//  NSFetchedResultsController.
//
//  Created by David McKnight on 2016-02-10.
//  Copyright (C) 2016 David McKnight actual@oswind.xyz
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#import <UIKit/UIKit.h>
#import "StockSearchViewDelegate.h"

#pragma mark - Public declaration

@interface StockWatchViewController : UIViewController
<StockSearchViewDelegate, NSFetchedResultsControllerDelegate,
UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate>

/* The application-shared main-queue managed Core Data context. */
@property (strong, nonatomic) NSManagedObjectContext * mainContext;

@end
