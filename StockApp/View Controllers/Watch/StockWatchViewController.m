//
//  StockWatchViewController.m
//  StockApp
//
//  The controller for a view responsible for displaying watched stocks. These
//  stocks are persisted in Core Data. Responds to touch events on row entries
//  which will load the stock detail view. This controller also acts as a
//  delegate to StockSearchView implementing StockSearchViewDelegate. This
//  allows the controller to update Core Data when a stock is selected for
//  watching from StockSearchView. The table view is updated using a
//  NSFetchedResultsController.
//
//  Created by David McKnight on 2016-02-10.
//  Copyright (C) 2016 David McKnight actual@oswind.xyz
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#import "Reachability.h"
#import "StockWatchViewController.h"
#import "StockSearchViewController.h"
#import "StockDetailTabViewController.h"
#import "Stock.h"

#pragma mark - Private declaration

@interface StockWatchViewController ()

/* Show an activity indicator when StockWatchView is being updated. */
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityLoading;

/* The asynchronous Core Data context that merges back into mainContext. */
@property (strong, nonatomic) NSManagedObjectContext * context;

/* Access the search bar for initial configuration. */
@property (strong, nonatomic) IBOutlet UISearchBar * searchBar;

/* The table view representation of the currently watched stocks. */
@property (strong, nonatomic) IBOutlet UITableView * watchedStocks;

/* The stocks being persisted in Core Data and displayed in StockWatchView. */
@property (nonatomic, strong) NSFetchedResultsController * savedResults;

/* Indicates if savedResults should be filtered by search bar text. */
@property (atomic) BOOL watchedNeedsFiltering;

/* The predicate used to filter watched stocks by search bar contents. Uses the
 * current text of the UISearchBar to create a NSPredicate. */
@property (nonatomic, strong) NSPredicate * watchedFilter;

/* The currently selected stock from the table view after cell selection. */
@property (nonatomic, strong) Stock * selectedStock;

/* Filter the contents of the table view using the contents of the string in
 * parameter `filter`. When filter is a non-empty string the table view will
 * show stocks which contain the filter string. When filter is empty the table
 * view shows all watched stocks currently persisted or cached by Core Data. */
- (void) filterWatchedWithText:(NSString *)filter;

/* Explicitly reload the table view of watched stocks. Sends a fetch request to
 * Core Data and then reloads the table view. This is intended to be used where
 * the fetch request needs to filter results and the delegate methods are not
 * yet available. */
- (void) reloadWatchedStocks;

/* Persist any changes to Core Data. */
- (void) mergeChangesToContext;

/* Request saved results from Core Data. */
- (void) getSavedResults;

/* Create and return a fetch request that queries for Stock entity in Core Data.
 * The parameter `predicate` contains an optional predicate to apply to the
 * fetch request. If the predicate is nil the returned fetch request will query
 * for all managed objects of Stock entity. */
- (NSFetchRequest *) getFetchRequestWithPredicate:(NSPredicate *)predicate;

/* Returns the currently selected stock symbol in the table view. Note when 
 * obtaining the selected stock the property indexPathForSelectedRow is used 
 * from the table view. The selected cell is then cleared. Thus this method
 * cannot be used more than once for each cell selection. */
- (Stock *) getSelectedStock;

/* Checks if the internet is reachable. Returns YES if internet was reachable.
 * Otherwise returns NO. */
- (BOOL) internetIsReachable;

/* Displays an alert indicating the network is offline. This is expected to be
 * used after checking internet reachability via internetIsReachable. When the
 * add button is pressed to load StockSearchView a segue is triggered and this
 * can be used instead of proceeding with the segue. */
- (void) alertForAddWithNetworkOffline;

/* Displays an alert indicating the network is offline. This is expected to be
 * used after checking internet reachability and with a check for the existence 
 * of a previous stock status. When the cell is pressed to load StockDetailView 
 * a segue is triggered and this can be used instead of proceeding with the 
 * segue. */
- (void) alertForDetailWithNetworkOffline;

/* Displays an alert indicating the network is offline. The parameter `message`
 * contains a message to display in the alert. */
- (void) displayNetworkOfflineAlertWithMessage:(NSString *)message;

/* Query Core Data for the count of entity having symbol matching the parameter
 * `checkSymbol` provided. Creates a predicate and fetch request but does not
 * execute the fetch. Instead issues a countForFetchRequest to the context.
 * Returns YES if the symbol exists in watched symbols. Otherwise returns NO. */
- (BOOL) symbolIsAlreadyWatched:(NSString *)checkSymbol;

@end

#pragma mark - Public implementation

@implementation StockWatchViewController

/* Load the Yahoo webpage per attribution requirements. */
- (IBAction)btnAttribution:(UIButton *)sender {
    [[UIApplication sharedApplication]
            openURL:[NSURL URLWithString:@"https://www.yahoo.com/?ilc=401"]];
}

/* Do any additional setup after loading the view. */
- (void)viewDidLoad {
    [super viewDidLoad];
    /* Default to showing all watched stocks. */
    self.watchedNeedsFiltering = NO;
    /* Configure the search bar. */
    [self.searchBar setReturnKeyType:UIReturnKeyDone];
    [self.searchBar setEnablesReturnKeyAutomatically:NO];
    [self.searchBar setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    /* Load the table view. */
    [self getSavedResults];
}

/* The asynchronous Core Data context that merges back into mainContext. */
- (NSManagedObjectContext *) context {
    if (_context == nil) {
        /* Create the asynchronous context. */
        _context = [[NSManagedObjectContext alloc]
                        initWithConcurrencyType:NSMainQueueConcurrencyType];
        /* Merge back into the application's main-queue context. */
        [_context setParentContext:self.mainContext];
    }
    return _context;
}

/* Dispose of any resources that can be recreated. */
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    self.savedResults = nil; /* Unload fetched results. */
}

/* Checks if the internet is reachable. Returns YES if internet was reachable.
 * Otherwise returns NO. */
- (BOOL) internetIsReachable {
    /* Using Apple's Reachability API. */
    Reachability * reachability =
        [Reachability reachabilityForInternetConnection];
    /* Get the current network status. */
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    /* Check if networkStatus indicates internet is not reachable. */
    if (networkStatus == NotReachable) {
        return NO; /* Internet not reachable. */
    }
    /* Internet is reachable. No guarantee is made that content can be read. */
    return YES;
}

/* Displays an alert indicating the network is offline. This is expected to be
 * used after checking internet reachability via internetIsReachable. When the
 * add button is pressed to load StockSearchView a segue is triggered and this
 * can be used instead of proceeding with the segue. */
- (void) alertForAddWithNetworkOffline {
    /* Create an alert that during add segue the network appears offline. */
    NSString * message = @"No internet connection detected. An internet "
            "connection is required for search functionality.";
    [self displayNetworkOfflineAlertWithMessage:message];
}

/* Displays an alert indicating the network is offline. This is expected to be
 * used after checking internet reachability and with a check for the existence
 * of a previous stock status. When the cell is pressed to load StockDetailView
 * a segue is triggered and this can be used instead of proceeding with the
 * segue. */
- (void) alertForDetailWithNetworkOffline {
    /* Create an alert that during detail segue the network appears offline. */
    NSString * message = @"No internet connection detected and no previous "
            "stock status was found. An internet connection is required to "
            "load a stock status for the first time.";
    [self displayNetworkOfflineAlertWithMessage:message];
}

/* Displays an alert indicating the network is offline. The parameter `message`
 * contains a message to display in the alert. */
- (void) displayNetworkOfflineAlertWithMessage:(NSString *)message {
    /* Create an alert indicating the network appears offline. */
    UIAlertController * alert =
            [UIAlertController alertControllerWithTitle:@"Network Offline"
                message:message preferredStyle:UIAlertControllerStyleAlert];
    /* Dismiss the alert when ok is pressed. */
    UIAlertAction * dismiss = [UIAlertAction actionWithTitle:@"OK"
            style:UIAlertActionStyleDefault
            handler:^(UIAlertAction * action) {
                [self dismissViewControllerAnimated:YES completion:nil];
            }];
    /* Add the default action to the alert. */
    [alert addAction:dismiss];
    /* Display the alert. */
    [self presentViewController:alert animated:YES completion:nil];
}

/* Persist any changes to Core Data. */
- (void) mergeChangesToContext {
    /* Check for pending changes. */
    if (!self.context.hasChanges) {
        return; /* Only save if there are pending changes. */
    }
    NSError * error;
    /* Save the asynchronous context. */
    BOOL saved = [self.context save:&error];
    if (!saved) {
        NSLog(@"Core Data did not save: %@", [error localizedDescription]);
    } else {
        /* Merge pending changes back to main-queue. */
        saved = [self.mainContext save:&error];
        if (!saved) {
            NSLog(@"Core Data did not merge back pending changes: %@",
                  [error localizedDescription]);
        }
    }
}

/* Request saved results from Core Data. */
- (void) getSavedResults {
    [self.activityLoading startAnimating];
    NSError * error;
    BOOL fetched = [self.savedResults performFetch:&error];
    if (!fetched) {
        NSLog(@"Core Data did not fetch saved results: %@",
              [error localizedDescription]);
    }
    [self.activityLoading stopAnimating];
}

/* Explicitly reload the table view of watched stocks. Sends a fetch request to
 * Core Data and then reloads the table view. This is intended to be used where
 * the fetch request needs to filter results and the delegate methods are not
 * yet available. */
- (void) reloadWatchedStocks {
    /* Query Core Data for saved results. The result of this message may differ
     * depending on if the fetch request has been changed. */
    [self getSavedResults];
    [self.watchedStocks reloadData]; /* Reload the table view. */
}

/* Create and return a fetch request that queries for Stock entity in Core Data.
 * The parameter `predicate` contains an optional predicate to apply to the
 * fetch request. If the predicate is nil the returned fetch request will query
 * for all managed objects of Stock entity. */
- (NSFetchRequest *) getFetchRequestWithPredicate:(NSPredicate *)predicate {
    /* Create the fetch request associated with the controller. */
    NSFetchRequest * request = [[NSFetchRequest alloc] init];
    /* Create the fetched entity of type Stock. */
    NSEntityDescription * stocks =
        [NSEntityDescription entityForName:@"Stock"
                inManagedObjectContext:self.context];
    /* Specify results sorted ascending order by symbol. */
    NSSortDescriptor * sortBySymbol =
        [[NSSortDescriptor alloc] initWithKey:@"symbol" ascending:YES];
    /* Setup the fetch request entity, sort, and predicate. */
    [request setEntity:stocks];
    [request setSortDescriptors:[NSArray arrayWithObject:sortBySymbol]];
    [request setPredicate:predicate]; /* Optional. May be nil. */
    
    return request; /* Return the configured fetch request. */
}

/* Query Core Data for the count of entity having symbol matching the parameter
 * `checkSymbol` provided. Creates a predicate and fetch request but does not
 * execute the fetch. Instead issues a countForFetchRequest to the context. 
 * Returns YES if the symbol exists in watched symbols. Otherwise returns NO. */
- (BOOL) symbolIsAlreadyWatched:(NSString *)checkSymbol {
    /* Construct a fetch request for the check. */
    NSFetchRequest * request = [[NSFetchRequest alloc] init];
    NSEntityDescription * stocks =
        [NSEntityDescription entityForName:@"Stock"
                inManagedObjectContext:self.context];
    /* Construct a predicate matching the symbol to check. */
    NSPredicate * check =
        [NSPredicate predicateWithFormat:@"symbol == %@", checkSymbol];
    /* Setup the fetch request with the predicate. */
    [request setEntity:stocks];
    [request setIncludesPendingChanges:NO];
    [request setPredicate:check];
    /* Check the count that would be returned if the fetch were executed. */
    NSError * error;
    NSUInteger count =
        [self.context countForFetchRequest:request error:&error];
    if (count == NSNotFound) {
        /* NSNotFound from countForFetchRequest indicates an error. */
        NSLog(@"Core Data failure while checking symbol existence: %@",
              [error localizedDescription]);
    }
    
    return (count == 0)? NO: YES; /* Only add symbols if count is zero. */
}

/* The predicate used to filter watched stocks by search bar contents. Uses the
 * current text of the UISearchBar to create a NSPredicate. */
- (NSPredicate *) watchedFilter {
    return [NSPredicate predicateWithFormat:
            @"symbol contains[cd] %@ OR desc contains[cd] %@",
            self.searchBar.text,self.searchBar.text];
}

/* Get the fetched results controller that handles saved results. */
- (NSFetchedResultsController *) savedResults {
    /* If unset the controller should contain all watched stocks. If the 
     * controller is set (not nil) and watchedNeedsFiltering is YES then the 
     * fetch request should be modified to return results with a predicate. */
    if (_savedResults == nil || self.watchedNeedsFiltering) {
        NSFetchRequest * request; /* Use internal state to setup request. */
        if (_savedResults == nil) {
            /* Create a fetch request for all Stock entity. */
            request = [self getFetchRequestWithPredicate:nil];
        } else if (self.watchedNeedsFiltering) {
            /* Create a fetch request for filtered Stock entity. */
            request = [self getFetchRequestWithPredicate:self.watchedFilter];
            self.watchedNeedsFiltering = NO; /* Finished filter setup. */
        }
        _savedResults = [[NSFetchedResultsController alloc]
                         initWithFetchRequest:request
                         managedObjectContext:self.context
                           sectionNameKeyPath:nil /* One section. */
                                    cacheName:nil];
        _savedResults.delegate = self;
    }
    /* If the controller is set and watchedNeedsFiltering is NO just return the
     * current controller. */
    return _savedResults;
}

/* Filter the contents of the table view using the contents of the string in 
 * parameter `filter`. When filter is a non-empty string the table view will
 * show stocks which contain the filter string. When filter is empty the table
 * view shows all watched stocks currently persisted or cached by Core Data. */
- (void) filterWatchedWithText:(NSString *)filter {
    if (filter == nil || filter.length == 0) {
        self.savedResults = nil;          /* Need to unfilter watched stocks. */
    } else {
        self.watchedNeedsFiltering = YES;   /* Need to filter watched stocks. */
    }
    /* Reload the table view of watched stocks. */
    [self reloadWatchedStocks];
}

/* Returns the currently selected stock symbol in the table view. Note when
 * obtaining the selected stock the property indexPathForSelectedRow is used
 * from the table view. The selected cell is then cleared. Thus this method
 * cannot be used more than once for each cell selection. */
- (Stock *) getSelectedStock {
    /* Get the currently selected stock using the table view indexPath. */
    Stock * selected = [self.savedResults objectAtIndexPath:
            self.watchedStocks.indexPathForSelectedRow];
    /* Get the selected table view cell. */
    UITableViewCell * cell = [self.watchedStocks
            cellForRowAtIndexPath:self.watchedStocks.indexPathForSelectedRow];
    /* De-select the cell. */
    cell.selected = NO;
    /* Return the selected stock symbol. */
    return selected;
}

#pragma mark - UITableViewDataSource

/* Each cell represents a stock being watched. */
- (UITableViewCell *)tableView:(UITableView *)tableView
                     cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell * cell = /* StockWatchView uses identifier `symbol`. */
    [tableView dequeueReusableCellWithIdentifier:@"symbol"
                                    forIndexPath:indexPath];
    /* Get the managed Stock object at the specified index path. */
    Stock * stock = [self.savedResults objectAtIndexPath:indexPath];
    /* Setup the cell view using the managed Stock object. */
    cell.textLabel.text = stock.symbol;
    cell.detailTextLabel.text = stock.desc;
    
    return cell;
}

/* Number of rows corresponds to number of watched stocks in saved results. */
- (NSInteger)tableView:(UITableView *)tableView
             numberOfRowsInSection:(NSInteger)section {
    return [[self.savedResults.sections objectAtIndex:section] numberOfObjects];
}

/* Support the removal of table entries. */
- (void)tableView:(UITableView *)tableView
        commitEditingStyle:(UITableViewCellEditingStyle)editingStyle
        forRowAtIndexPath:(NSIndexPath *)indexPath {
    /* Only concerned with deleting entries. */
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        /* Get a reference to the removed Stock. */
        Stock * removed = [self.savedResults objectAtIndexPath:indexPath];
        /* Remove the stock and save the change. */
        [self.context deleteObject:removed];
        [self mergeChangesToContext];
    }
}

#pragma mark - NSFetchedResultsControllerDelegate

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    /* Notify the table view that content will be changing. */
    [self.watchedStocks beginUpdates];
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    /* Notify the table view that content changes are complete. */
    [self.watchedStocks endUpdates];
}

/* Update the table view when Core Data changes. */
- (void)controller:(NSFetchedResultsController *)controller
        didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath
        forChangeType:(NSFetchedResultsChangeType)type
        newIndexPath:(NSIndexPath *)newIndexPath {
    /* Get a reference to the table view being changed. */
    UITableView * tableView = self.watchedStocks;
    /* Modify the table view based on the type of change.*/
    switch(type) {
        /* Insert a new row when a search is completed. */
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:
                     [NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
        /* Delete a row when a watched stock is removed. */
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:
                     [NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
        /* Change the content of a row. Usually this means the Status entity 
         * relationship has been updated. As such there's nothing to do here 
         * because the cell for the watched stock is already handled by the 
         * UITableView method cellForRowAtIndexPath during the event 
         * NSFetchedResultsChangeInsert or NSFetchedResultsChangeMove. */
        case NSFetchedResultsChangeUpdate:
            /* Reload the cell using UITableView method reloadRowsAtIndexPaths. 
             * This will reload the cell contents using cellForRowAtIndexPath.
             * NOTE: Only uncomment this if you know the cell content at the
             *       index path has changed. This probably won't happen for
             *       watched stocks. */
            //[tableView reloadRowsAtIndexPaths:
            //         [NSArray arrayWithObject:indexPath]
            //                 withRowAnimation:UITableViewRowAnimationFade];
            break;
        /* Moved a row from old position to new position. */
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:
                     [NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            /* Insert the row at it's final position with content updated using
             * the table view's datasource. */
            [tableView insertRowsAtIndexPaths:
                     [NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

#pragma mark - StockSearchViewDelegate

/* A stock has been selected in StockSearchView which should be persisted in
 * Core Data. The parameter `symbol` contains the selected stock symbol and
 * `description` contains the stock company name. Delegate controllers should
 * check if the symbol is already watched. If not then create a new managed
 * object of type Stock to persist the watched stock data. */
- (void) searchDidFinishWithSymbol:(NSString *)symbol
                    andDescription:(NSString *)description {
    [self.activityLoading startAnimating]; /* Start update indicator. */
    /* Save the changes to Core Data if the symbol is not watched. */
    if (![self symbolIsAlreadyWatched:symbol]) {
        /* The selected stock is not already watched. Create a new managed Stock
         * object to be persisted in Core Data. */
        Stock * stock = [NSEntityDescription
                            insertNewObjectForEntityForName:@"Stock"
                            inManagedObjectContext:self.context];
        /* Prepare the Stock attributes to be persisted. */
        stock.symbol = symbol;
        stock.desc = description;
        /* Save the new managed Stock object to Core Data. */
        [self mergeChangesToContext];
    }
    /* Dismiss StockSearchView. */
    [self dismissViewControllerAnimated:YES completion:^{
        [self.activityLoading stopAnimating]; /* Stop the update indicator. */
    }];
}

/* StockSearchView has been cancelled without selecting a stock to watch. */
- (void) searchDidCancel {
    /* Dismiss StockSearchView. */
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Navigation Segue

/* Check if a segue to StockSearchView or StockDetailView should be allowed to 
 * proceed. For StockSearchView with identifier `add` check for an internet
 * connection. For StockDetailView with identifier `detail` in addition to the
 * internet connection also check if a previous stock status has been stored in 
 * Core Data. */
- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier
                                  sender:(id)sender {
    /* Check the segue to based on the identifier being `add` or `detail`. */
    if ([identifier isEqualToString:@"add"]) {
        /* Check if the internet is reachable before loading StockSearchView. */
        if ([self internetIsReachable] == NO) {
            /* Alert for no network connection to use with stock search. */
            [self alertForAddWithNetworkOffline];
            return NO; /* Do not proceed with the segue. */
        }
    } else if ([identifier isEqualToString:@"detail"]) {
        /* Get the watched stock which was selected from the table view. */
        self.selectedStock = [self getSelectedStock];
        /* If no stock status currently exists in Core Data check reachability
         * before the segue. A network connection is required to fill the
         * StockDetailView anyway. */
        if (self.selectedStock.stockStatus == nil
                   && [self internetIsReachable] == NO) {
            /* Alert for no network connection and no previous stock status was
             * found. No point in continuing the segue in this state. */
            [self alertForDetailWithNetworkOffline];
            return NO; /* Do not proceed with the segue. */
        }
    }
    /* Initial validation passed. Allow the segue. */
    return YES;
}

/* Prepare the StockSearchView by getting a reference to it's controller.
 * Provide the controller with a reference to the managed object to be persisted
 * in Core Data. Set the delegate to this controller so it can respond to the
 * completion of updates. */
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    /* Check the segue identifier to see which kind of segue is being triggered.
     * The identity is expected to be either `add` for adding new stocks to 
     * watch via StockSearchView, or `detail` to display a watched stock via
     * StockDetailView. */
    if ([segue.identifier isEqualToString:@"add"]) {
        /* Prepare the StockSearchView's controller. */
        StockSearchViewController * search =
                    [segue destinationViewController];
        /* Notify this controller when a search is finished. This allows the
         * search result to be persisted in Core Data and displayed in the
         * StockWatchView. */
        search.delegate = self;       /* Notify this controller. */
    } else if ([segue.identifier isEqualToString:@"detail"]) {
        /* Internet is reachable or a previous stock status exists. Prepare
         * for the segue by configuring the StockDetailTabView's controller. */
        StockDetailTabViewController * detailTabView =
                    [segue destinationViewController];
        /* Provide the application shared asynchronous managed context for 
         * merging Status entities. */
        detailTabView.context = self.context;
        /* Provide the selected stock to StockDetailTabView to have the last
         * status loaded from Core Data (if it exists) or read from the
         * content provider (Yahoo! Finance) as required. */
        detailTabView.watchedSymbol = self.selectedStock;
    }
}

#pragma mark - UISearchBarDelegate

/* Editing should begin when the search bar is selected. */
- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    return YES;
}

/* Filter watched stocks displayed using search bar text. */
- (void)searchBar:(UISearchBar *)searchBar
        textDidChange:(NSString *)searchText {
    [self filterWatchedWithText:searchText]; /* Filter watched stocks. */
}

/* Remove the onscreen keyboard when the `done` button is pressed. */
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder]; /* Remove the keyboard. */
}

@end
