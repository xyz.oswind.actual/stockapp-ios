//
//  AppNavigationController.h
//  StockApp
//
//  This application uses a navigation controller as the first view controller
//  presented. This means the navigation controller handles certain view events
//  application-wide. This class provides an override point for these events.
//  In particular it is desirable to control rotation for some child-view
//  controllers while allowing unrestricted rotation for the rest.
//
//  Created by David McKnight on 3/18/16.
//  Copyright (C) 2016 David McKnight actual@oswind.xyz
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#import <UIKit/UIKit.h>

#pragma mark - Public declaration

@interface AppNavigationController : UINavigationController

@end
