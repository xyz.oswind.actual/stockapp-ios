//
//  AppNavigationController.m
//  StockApp
//
//  This application uses a navigation controller as the first view controller
//  presented. This means the navigation controller handles certain view events
//  application-wide. This class provides an override point for these events.
//  In particular it is desirable to control rotation for some child-view
//  controllers while allowing unrestricted rotation for the rest.
//
//  Created by David McKnight on 3/18/16.
//  Copyright (C) 2016 David McKnight actual@oswind.xyz
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#import "AppNavigationController.h"

#pragma mark - Private declaration

@interface AppNavigationController ()

@end

#pragma mark - Public implementation

@implementation AppNavigationController

/* Restrict the supported rotation only for some views. The supported rotation
 * application-wide is any device-supported orientation. Only particular view
 * controllers will deviate from this default. For these view controllers this 
 * method will return NO based on their preference. These view controllers can 
 * then override supportedInterfaceOrientations and
 * preferredInterfaceOrientationForPresentation enforcing rotation control. */
- (BOOL) shouldAutorotate {
    /* Check the presented view controller's auto-rotation preference. If the
     * presented view controller returns YES accept rotation. Otherwise prevent
     * the rotation. */
    return [self.topViewController shouldAutorotate]; /* Enforce rotation. */
}

/* Check the orientations supported by the presented view controller. */
#if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
/* Return type is NSUInteger up to SDK version 8.4. */
- (NSUInteger) supportedInterfaceOrientations {
#else
/* Return type is UIInterfaceOrientationMask starting in SDK version 9. */
- (UIInterfaceOrientationMask) supportedInterfaceOrientations {
#endif
    return [self.topViewController supportedInterfaceOrientations];
}

/* Check the preferred orientations of the presented view controller. */
- (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
    return [self.topViewController
            preferredInterfaceOrientationForPresentation];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
