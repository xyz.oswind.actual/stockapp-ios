//
//  StockDetailViewController.h
//  StockApp
//
//  The controller for a view responsible for displaying the currently selected
//  stock's status. If the stocks status doesn't exist it will request it and
//  persist the data in Core Data. Later loading of the view will first use the
//  saved status. Provides an option to update the status.
//
//  Created by David McKnight on 2016-02-10.
//  Copyright (C) 2016 David McKnight actual@oswind.xyz
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#import <UIKit/UIKit.h>
#import "StockStatusDelegate.h"
#import "Stock.h"

#pragma mark - Public declaration

@interface StockDetailViewController : UIViewController <StockStatusDelegate>

/* The application shared asynchronous Core Data context that merges back into 
 * the main-queue context. */
@property (strong, nonatomic) NSManagedObjectContext * context;

/* The watched symbol for which a status will be displayed in this view. This is
 * expected to be set during the segue from StockWatchView to this view. By 
 * setting this property during the segue the status can be loaded during the
 * viewDidLoad method. */
@property (strong, nonatomic) Stock * watchedSymbol;

/* The StockDetailTabViewController is monitoring reachability and has detected
 * a change in reachability via NSNotificationCenter. This method allows the
 * updated reachability to be acted upon in StockDetailView. This means changing
 * the update button to reflect the offline state and stopping activity 
 * indicators that may be running. */
- (void) updateViewWithReachability:(BOOL)internetIsReachable;

@end
