//
//  StockDetailViewController.m
//  StockApp
//
//  The controller for a view responsible for displaying the currently selected
//  stock's status. If the stocks status doesn't exist it will request it and
//  persist the data in Core Data. Later loading of the view will first use the
//  saved status. Provides an option to update the status.
//
//  Created by David McKnight on 2016-02-10.
//  Copyright (C) 2016 David McKnight actual@oswind.xyz
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#import "Reachability.h"
#import "StockDetailViewController.h"
#import "StockStatus.h"
#import "Status.h"
#import "Chart.h"
#import "Competitor.h"

#pragma mark - Private declaration

@interface StockDetailViewController ()

/* The stock status is embedded in a scroll view for better compatibility with
 * different sized iOS devices. In particular smaller than an iPhone 6. */
@property (strong, nonatomic) IBOutlet UIScrollView *scrollStatusContent;

/* Show a graphic arrow from assets based on stock status. */
@property (strong, nonatomic) IBOutlet UIImageView *imgChange;
@property (strong, nonatomic) IBOutlet UIImageView *imgPercentChange;

/* Request the latest status by querying the content provider. */
@property (strong, nonatomic) IBOutlet UIButton *btnUpdate;

/* Show an activity indicator when loading data into the view. */
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityLoading;

/* View outlets for displaying the current (or cached) status of a stock. These
 * may be populated using persisted data from Core Data or read from the content
 * provider (Yahoo! Finance) as required. This allows the application to run
 * without an active internet connection. */
@property (strong, nonatomic) IBOutlet UILabel *lblMarketCap;
@property (strong, nonatomic) IBOutlet UILabel *lblAvgVolume;
@property (strong, nonatomic) IBOutlet UILabel *lblVolume;
@property (strong, nonatomic) IBOutlet UILabel *lblYearRange;
@property (strong, nonatomic) IBOutlet UILabel *lblDayRange;
@property (strong, nonatomic) IBOutlet UILabel *lblYearEst;
@property (strong, nonatomic) IBOutlet UILabel *lblAsk;
@property (strong, nonatomic) IBOutlet UILabel *lblBid;
@property (strong, nonatomic) IBOutlet UILabel *lblOpen;
@property (strong, nonatomic) IBOutlet UILabel *lblPrevClose;
@property (strong, nonatomic) IBOutlet UILabel *lblCurrency;
@property (strong, nonatomic) IBOutlet UILabel *lblLastTradeTime;
@property (strong, nonatomic) IBOutlet UILabel *lblLastTradeDate;
@property (strong, nonatomic) IBOutlet UILabel *lblPercentChange;
@property (strong, nonatomic) IBOutlet UILabel *lblChange;
@property (strong, nonatomic) IBOutlet UILabel *lblName;

/* The StockStatus object to use when requesting updated status. */
@property (strong, nonatomic) StockStatus * status;

/* Configure the change label and image based on actual status. The parameter
 * `changeLabel` is expected to be a UILabel used to display a stock change
 * (either change or percent change). The parameter `changeImage` is expected to
 * be the UIImageView that corresponds to the UILabel. The parameter `status`
 * contains the status string to use when configuring both views. Checks the
 * leading character of the provided status string (+/-). Using this character
 * the UILabel is set either green, red, or default tint if neither. The UILabel
 * text is set to the modified NSString representation without +/-. The
 * UIImageView is set to the arrow (asset) that matches the change. */
- (void) configureChangeWithStatus:(NSString *)status
                          AndLabel:(UILabel *)changeLabel
                          AndImage:(UIImageView *)changeImage;

/* Configure the stock name label based on the name returned from the content 
 * provider. The parameter `name` contains the name string to use when 
 * configuring the label. Checks the length of the name string. If the name will
 * fit on one line then set alignment to center with an enlarged font (20).
 * Otherwise leave it at the default multi-line label and font size (17). */
- (void) configureNameWithString:(NSString *)name;

/* Checks if the internet is reachable. Returns YES if internet was reachable.
 * Otherwise returns NO. This method does not provide real-time updates. The
 * real-time updates are provided by StockDetailTabViewController. */
- (BOOL) internetIsReachable;

/* Configure the update button state depending on if the internet is reachable
 * or not. Enable the button if reachable. Disable it and change the text to
 * show internet unavailable. */
- (void) configureUpdateButtonWithReachability:(BOOL)internetReachable;

/* When a previous status exists load it into the view first. This allows the
 * view to provide information without an internet connection. It also helps to
 * prevent repeatedly requesting updates from the content provider. The 
 * parameter `last` contains a last status retrieved from Core Data. */
- (void) configureViewWithPreviousStatus:(Status *)last;

/* Check for updates from the content provider then configure the view. This
 * will send an update request for the currently displayed stock to the content
 * provider. Configure the view to reflect the update request. */
- (void) configureViewWithUpdates;

/* Limit requesting updates by disabling the update button after an update is
 * requested. Visually changes the appearance to show the button is disabled.
 * Restores the update button functionality after ~30 seconds. */
- (void) rateLimitUpdates;

/* Insert a managed Status object corresponding to the StockStatus parameter
 * `stockStatus`. The parameter must contain a valid StockStatus object for a
 * valid stock symbol. Minimal validation of the StockStatus is performed. */
- (void) insertStatusWithStockStatus:(StockStatus *)stockStatus;

/* Merge changes in the asynchronous context back to the main-queue context. */
- (void) mergeBackToMainContext;

@end

#pragma mark - Public implementation

@implementation StockDetailViewController

/* Support only the portrait orientation for this view. */
#if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
/* Return type is NSUInteger up to SDK version 8.4. */
- (NSUInteger) supportedInterfaceOrientations {
#else
/* Return type is UIInterfaceOrientationMask starting in SDK version 9. */
- (UIInterfaceOrientationMask) supportedInterfaceOrientations {
#endif    
    return UIInterfaceOrientationMaskPortrait;
}

/* Support the portrait orientation. */
- (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationPortrait;
}

/* Disable rotation unless the view is loaded and not portrait orientation. */
- (BOOL) shouldAutorotate {
    /* Only enable rotation if orientation is not portrait. */
    if ([[UIApplication sharedApplication] statusBarOrientation]
        != UIInterfaceOrientationPortrait) {
        return YES; /* View not in portrait orientation. */
    }
    return NO;
}

/* Load the Yahoo webpage per attribution requirements. */
- (IBAction)btnAttribution:(UIButton *)sender {
    [[UIApplication sharedApplication]
            openURL:[NSURL URLWithString:@"https://www.yahoo.com/?ilc=401"]];
}

/* Request the latest status by querying the content provider. */
- (IBAction)btnUpdate:(UIButton *)sender {
    /* Start the activity indicator. */
    [self.activityLoading startAnimating];
    /* Request the latest stock status. */
    [self configureViewWithUpdates];
}

/* Insert a managed Status object corresponding to the StockStatus parameter
 * `stockStatus`. The parameter must contain a valid StockStatus object for a
 * valid stock symbol. Minimal validation of the StockStatus is performed. */
- (void) insertStatusWithStockStatus:(StockStatus *)stockStatus {
    /* The parameter must be allocated and contain a status. */
    if (stockStatus == nil || stockStatus.symbol == nil) {
        return;
    }
    /* Insert the new managed Stock object asynchronously. */
    [self.context performBlock:^{
        /* If a previous status exists load it by faulting. */
        Status * status;
        if (self.watchedSymbol.stockStatus != nil) {
            /* A Status managed object exists. */
            status = self.watchedSymbol.stockStatus;
        } else {
            /* No previous Status. Create the managed object. */
            status = [NSEntityDescription
                      insertNewObjectForEntityForName:@"Status"
                      inManagedObjectContext:self.context];
        }
        /* If a previous chart exists load it by faulting. */
        Chart * chart;
        if (status.statusChart != nil) {
            /* A Chart managed object exists. */
            chart = status.statusChart;
        } else {
            /* No previous Chart. Create the managed object. */
            chart = [NSEntityDescription
                     insertNewObjectForEntityForName:@"Chart"
                     inManagedObjectContext:self.context];
        }
        /* If previous competitor data exists load it by faulting. Clear the 
         * previously stored data if it already exists. */
        NSSet * competitors = status.statusCompetitors;
        if (competitors != nil && competitors.count > 0) {
            /* Clear the existing competitor data. */
            for (Competitor * competitor in competitors) {
                [self.context deleteObject:competitor];
            }
        }
        /* Setup the managed Competitor objects from the up-to-date remote
         * server data. */
        for (NSArray * entry in stockStatus.comparisonData) {
            Competitor * competitor =
                [NSEntityDescription
                    insertNewObjectForEntityForName:@"Competitor"
                    inManagedObjectContext:self.context];
            competitor.symbol = entry[0];
            competitor.name = entry[1];
            competitor.change = entry[2];
            competitor.marketCap = entry[3];
            /* Setup the Status<->Competitor relationships. */
            [status addStatusCompetitorsObject:competitor];
            competitor.competitorStatus = status;
        }
        /* Setup the managed Status object. */
        status.symbol = stockStatus.symbol;
        status.name = stockStatus.desc;
        status.change = stockStatus.change;
        status.percentChange = stockStatus.percentChange;
        status.lastTradeTime = stockStatus.lastTradeTime;
        status.lastTradeDate = stockStatus.lastTradeDate;
        status.currency = stockStatus.currency;
        /* Force 2-digit precision for attribute prevClose. */
        status.prevClose =
            [NSString stringWithFormat:@"%.2f",
                [stockStatus.prevClose floatValue]];
        /* Force 2-digit precision for attribute open. */
        status.open =
            [NSString stringWithFormat:@"%.2f", [stockStatus.open floatValue]];
        /* Force 2-digit precision for attribute bid. */
        status.bid =
            [NSString stringWithFormat:@"%.2f", [stockStatus.bid floatValue]];
        /* Force 2-digit precision for attribute ask. */
        status.ask =
            [NSString stringWithFormat:@"%.2f", [stockStatus.ask floatValue]];
        /* Force 2-digit precision for attribute yearEst. */
        status.yearEst =
            [NSString stringWithFormat:@"%.2f",
                [stockStatus.yearTargetEst floatValue]];
        status.yearRange = stockStatus.yearsRange;
        status.dayRange = stockStatus.daysRange;
        status.volume = stockStatus.volume;
        status.avgVolume = stockStatus.avgVolume;
        status.marketCap = stockStatus.marketCap;
        /* Setup the managed Chart object. */
        chart.data = stockStatus.chartData;
        /* Setup the Status<->Stock relationships. */
        status.watchedStock = self.watchedSymbol;
        self.watchedSymbol.stockStatus = status;
        /* Setup the Status<->Chart relationships. */
        status.statusChart = chart;
        chart.chartStatus = status;
        /* Save the stock status to Core Data. */
        NSError * error;
        if (![self.context save:&error]) {
            NSLog(@"Core Data did not save the stock status: %@\n%@",
                  [error localizedDescription], [error userInfo]);
        }
        [self mergeBackToMainContext];
    }];
}

/* When a previous status exists load it into the view first. This allows the
 * view to provide information without an internet connection. It also helps to
 * prevent repeatedly requesting updates from the content provider. The
 * parameter `last` contains a last status retrieved from Core Data. */
- (void) configureViewWithPreviousStatus:(Status *)last {
    [self.activityLoading startAnimating]; /* Start activity indicator. */
    self.lblMarketCap.text = last.marketCap;
    self.lblAvgVolume.text = last.avgVolume;
    self.lblVolume.text = last.volume;
    self.lblYearRange.text = last.yearRange;
    self.lblDayRange.text = last.dayRange;
    self.lblYearEst.text = last.yearEst;
    self.lblAsk.text = last.ask;
    self.lblBid.text = last.bid;
    self.lblOpen.text = last.open;
    self.lblPrevClose.text = last.prevClose;
    self.lblCurrency.text = last.currency;
    self.lblLastTradeTime.text = last.lastTradeTime;
    self.lblLastTradeDate.text = last.lastTradeDate;
    /* Configure the percent change label based on status. */
    [self configureChangeWithStatus:last.percentChange
                           AndLabel:self.lblPercentChange
                           AndImage:self.imgPercentChange];
    /* Configure the change label based on status. */
    [self configureChangeWithStatus:last.change
                           AndLabel:self.lblChange
                           AndImage:self.imgChange];
    /* For short symbol names make alignment center and font larger. */
    [self configureNameWithString:last.name];
    [self.activityLoading stopAnimating]; /* Stop activity indicator. */
}

/* Check for updates from the content provider then configure the view. This
 * will send an update request for the currently displayed stock to the content
 * provider. Configure the view to reflect the update request. */
- (void) configureViewWithUpdates {
    /* Request updates for the currently displayed stock. */
    [self.status getStatusWithSymbol:self.watchedSymbol.symbol];
    /* Now disable the button for ~30s to prevent hammering the provider. */
    [self rateLimitUpdates];
}

/* Merge changes in the asynchronous context back to the main-queue context. */
- (void) mergeBackToMainContext {
    /* Check for pending changes. */
    if (!self.context.parentContext.hasChanges) {
        return; /* Only save if there are pending changes. */
    }
    NSError * error;
    BOOL saved = [self.context.parentContext save:&error];
    if (!saved) {
        NSLog(@"Core Data did not merge back to main context: %@",
              [error localizedDescription]);
    }
}

/* Limit requesting updates by disabling the update button after an update is
 * requested. Visually changes the appearance to show the button is disabled.
 * Restores the update button functionality after ~30 seconds. */
- (void) rateLimitUpdates {
    /* Change visual state and interaction state to disabled. */
    [self.btnUpdate setEnabled:NO];
    self.btnUpdate.layer.backgroundColor = [[UIColor lightGrayColor] CGColor];
    /* Re-enable the button after ~30s. */
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 30 * NSEC_PER_SEC),
                   dispatch_get_main_queue(), ^{
        [self.btnUpdate setEnabled:YES];
        self.btnUpdate.layer.backgroundColor = [self.view.tintColor CGColor];
    });
}

/* Configure the update button view depending on if the internet is reachable
 * or not. Enable the button if reachable. Disable it and change the text to
 * show internet unavailable. */
- (void) configureUpdateButtonWithReachability:(BOOL)internetReachable {
    /* Re-configure the update button using reachability state. */
    if (internetReachable == YES) {
        /* Configure enabled UIButton view color. */
        self.btnUpdate.layer.backgroundColor = [self.view.tintColor CGColor];
        /* Enable update as internet is reachable. */
        [self.btnUpdate setEnabled:YES];
        /* Set the title. */
        [self.btnUpdate setTitle:@"Update Now" forState:UIControlStateNormal];
    } else {
        /* Configure disabled UIButton view color. */
        self.btnUpdate.layer.backgroundColor =
            [[UIColor lightGrayColor] CGColor];
        /* Disable update as internet is not reachable. */
        [self.btnUpdate setEnabled:NO];
        /* Set the title. */
        [self.btnUpdate setTitle:@"No Network" forState:UIControlStateNormal];
    }
}

/* Checks if the internet is reachable. Returns YES if internet was reachable.
 * Otherwise returns NO. This method does not provide real-time updates. The
 * real-time updates are provided by StockDetailTabViewController. */
- (BOOL) internetIsReachable {
    /* Using Apple's Reachability API. */
    Reachability * reachability =
            [Reachability reachabilityForInternetConnection];
    /* Get the current network status. */
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    /* Check if networkStatus indicates internet is not reachable. */
    if (networkStatus == NotReachable) {
        return NO; /* Internet not reachable. */
    }
    /* Internet is reachable. No guarantee is made that content can be read. */
    return YES;
}

/* Configure the change label and image based on actual status. The parameter
 * `changeLabel` is expected to be a UILabel used to display a stock change
 * (either change or percent change). The parameter `changeImage` is expected to
 * be the UIImageView that corresponds to the UILabel. The parameter `status` 
 * contains the status string to use when configuring both views. Checks the
 * leading character of the provided status string (+/-). Using this character 
 * the UILabel is set either green, red, or default tint if neither. The UILabel
 * text is set to the modified NSString representation without +/-. The 
 * UIImageView is set to the arrow (asset) that matches the change. */
- (void) configureChangeWithStatus:(NSString *)status
                          AndLabel:(UILabel *)changeLabel
                          AndImage:(UIImageView *)changeImage {
    /* Get the first character from the status string provided. */
    unichar first = [status characterAtIndex:0];
    /* Check if the status string contains a leading +/-. */
    if (first == '+' || first == '-') {
        if (first == '+') {
            /* Positive change. */
            changeLabel.textColor = [UIColor greenColor];
            changeImage.image = [UIImage imageNamed:@"up"];
        } else if (first == '-') {
            /* Negative change. */
            changeLabel.textColor = [UIColor redColor];
            changeImage.image = [UIImage imageNamed:@"down"];
        }
        /* Set the label text minus the leading +/- with 2-digit precision. */
        changeLabel.text =
            [NSString stringWithFormat:@"%.2f", /* Set the precision... */
                [[status substringFromIndex:1] floatValue]]; /* On substring. */
    } else {
        /* Neither a +/- were found so just set the label text and precision. */
        changeLabel.text =
            [NSString stringWithFormat:@"%.2f", [status floatValue]];
        /* Remove any existing image. */
        changeImage.image = nil;
    }
}

/* Configure the stock name label based on the name returned from the content
 * provider. The parameter `name` contains the name string to use when
 * configuring the label. Checks the length of the name string. If the name will
 * fit on one line then set alignment to center with an enlarged font (20).
 * Otherwise leave it at the default multi-line label and font size (17). */
- (void) configureNameWithString:(NSString *)name {
    /* Configure the view independent from Interface Builder. */
    [self.lblName setNumberOfLines:1];
    [self.lblName setAdjustsFontSizeToFitWidth:true];
    [self.lblName setMinimumScaleFactor:0.75];
    /* Set the stock name label text. */
    self.lblName.text = name;
}

/* Get a StockStatus object for requesting updated stock status. */
- (StockStatus *) status {
    if (_status == nil) {
        _status = [[StockStatus alloc] init];
        _status.delegate = self; /* Notify this controller on completion. */
    }
    return _status;
}

/* Do any additional setup after loading the view. */
- (void)viewDidLoad {
    [super viewDidLoad];
    /* Set the title to the stock symbol. Note that setting the title of the
     * navigation item will not work when nested inside a tab bar. */
    self.tabBarController.title = self.watchedSymbol.symbol;
    /* Modify the activity indicator for loading. Make it larger. */
    CGAffineTransform scale = CGAffineTransformMakeScale(2.5,2.5);
    self.activityLoading.transform = scale;
    /* Configure the unchanging update button properties. */
    self.btnUpdate.layer.cornerRadius = 10.0f;
    [self.btnUpdate setTitleColor:[UIColor whiteColor]
                         forState:UIControlStateNormal];
    /* Configure the reachability-based properties of the update button for the 
     * first time when the view loads. */
    [self configureUpdateButtonWithReachability:[self internetIsReachable]];
    /* Check if a previous stock status exists. If it does exist load it.
     * Otherwise check if the internet is reachable and load it for the first
     * time from the content provider. */
    if (self.watchedSymbol.stockStatus == nil
        && [self internetIsReachable]) {
        /* Show the load indicator. */
        [self.activityLoading startAnimating];
        /* No previous stock status and internet is reachable. Read the stock
         * status from the content provider. */
        [self configureViewWithUpdates];
    } else {
        /* A previous stock status exists. Load the previous status. The Stock
         * contains a relationship representing a fault. Requesting the Status
         * fires the fault. Effectively lazy-loading the Status. */
        Status * previous = self.watchedSymbol.stockStatus;
        [self configureViewWithPreviousStatus:previous];
    }
}

/* Configure the view as it is being displayed after not having focus. */
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    /* Check the orientation and switch to portrait mode if required. Note this
     * will only happen if the device was previously landscape when this view is
     * appearing. Otherwise the device will remain locked in portrait mode. */
    if ([[UIApplication sharedApplication] statusBarOrientation]
        != UIInterfaceOrientationPortrait) {
        /* Not already in portrait mode. Change the device orientation. Note 
         * this works best in `did appear` as the view will have it's layout
         * configured. Running this in `will appear` or `did load` may not have
         * the layout configured so it will not work properly. */
        [[UIDevice currentDevice] setValue:
            [NSNumber numberWithInt:UIInterfaceOrientationPortrait]
                                    forKey:@"orientation"];
    }
    /* Scroll to the top of the current stock status. The content here is 
     * constrained to the top with an offset of zero. */
    [self.scrollStatusContent setContentOffset:CGPointZero animated:YES];
}

/* Configure the view as another view is about to be displayed. */
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self mergeBackToMainContext]; /* Save any pending changes to Core Data. */
}

/* Dispose of any resources that can be recreated. */
- (void)didReceiveMemoryWarning {
    self.status = nil; /* Allow StockStatus to unload. */
    [self mergeBackToMainContext]; /* Save any pending changes to Core Data. */
    [super didReceiveMemoryWarning];
}

/* The StockDetailTabViewController is monitoring reachability and has detected
 * a change in reachability via NSNotificationCenter. This method allows the
 * updated reachability to be acted upon in StockDetailView. This means changing
 * the update button to reflect the offline state and stopping activity
 * indicators that may be running. */
- (void) updateViewWithReachability:(BOOL)internetIsReachable {
    /* Update the UI state with the real-time reachability provided. */
    [self configureUpdateButtonWithReachability:internetIsReachable];
    /* Stop the activity indicator if the network is offline. */
    if (!internetIsReachable) {
        [self.activityLoading stopAnimating];
    }
}

#pragma mark - StockStatusDelegate

/* A status query which was previously submitted has now completed. Controllers
 * can now update their views with the StockStatus. */
- (void) statusDidComplete {
    /* The new stock status to be saved in Core Data. */
    StockStatus * newStatus = self.status;
    /* Update the view with current stock status. */
    self.lblMarketCap.text = newStatus.marketCap;
    self.lblAvgVolume.text = newStatus.avgVolume;
    self.lblVolume.text = newStatus.volume;
    self.lblYearRange.text = newStatus.yearsRange;
    self.lblDayRange.text = newStatus.daysRange;
    /* Force 2-digit precision for lblYearEst. */
    self.lblYearEst.text =
        [NSString stringWithFormat:@"%.2f",
            [newStatus.yearTargetEst floatValue]];
    /* Force 2-digit precision for lblAsk. */
    self.lblAsk.text =
        [NSString stringWithFormat:@"%.2f", [newStatus.ask floatValue]];
    /* Force 2-digit precision for lblBid. */
    self.lblBid.text =
        [NSString stringWithFormat:@"%.2f", [newStatus.bid floatValue]];
    /* Force 2-digit precision for lblOpen. */
    self.lblOpen.text =
        [NSString stringWithFormat:@"%.2f", [newStatus.open floatValue]];
    /* Force 2-digit precision for lblPrevClose. */
    self.lblPrevClose.text =
        [NSString stringWithFormat:@"%.2f", [newStatus.prevClose floatValue]];
    self.lblCurrency.text = newStatus.currency;
    self.lblLastTradeTime.text = newStatus.lastTradeTime;
    self.lblLastTradeDate.text = newStatus.lastTradeDate;
    /* Configure the percent change label based on status. */
    [self configureChangeWithStatus:newStatus.percentChange
                           AndLabel:self.lblPercentChange
                           AndImage:self.imgPercentChange];
    /* Configure the change label based on status. */
    [self configureChangeWithStatus:newStatus.change
                           AndLabel:self.lblChange
                           AndImage:self.imgChange];
    /* For short symbol names make alignment center and font larger. */
    [self configureNameWithString:newStatus.desc];
    /* Stop the activity indicator. */
    [self.activityLoading stopAnimating];
    /* Save the stock status. */
    [self insertStatusWithStockStatus:newStatus];
}

/* A status query which was previously submitted did not complete. The parameter
 * `error` contains the cause of the failure. */
- (void) statusDidNotCompleteWithError:(NSError *)error {
    NSLog(@"A session failed with client-side error code: %ld, "
          "and description: %@", (long)error.code,
          error.localizedDescription);
    [self.activityLoading stopAnimating];
}

@end
