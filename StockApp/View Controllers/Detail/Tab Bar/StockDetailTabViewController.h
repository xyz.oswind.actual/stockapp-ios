//
//  StockDetailTabViewController.h
//  StockApp
//
//  The controller for a view responsible for displaying a tabbed view
//  consisting of a stock's status. In particular current status, chart, and
//  competitor summary. This controller also monitors reachability and notifies
//  child views as required.
//
//  Created by David McKnight on 2016-03-04.
//  Copyright (C) 2016 David McKnight actual@oswind.xyz
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#import <UIKit/UIKit.h>
#import "Stock.h"

#pragma mark - Public declaration

@interface StockDetailTabViewController : UITabBarController
<UITabBarControllerDelegate>

/* The application shared asynchronous Core Data context that merges back into
 * the main-queue context. */
@property (strong, nonatomic) NSManagedObjectContext * context;

/* The watched symbol for which a status will be displayed in this view. This is
 * expected to be set during the segue from StockWatchView to this view. By
 * setting this property during the segue the status can be loaded during the
 * viewDidLoad method. */
@property (strong, nonatomic) Stock * watchedSymbol;

@end
