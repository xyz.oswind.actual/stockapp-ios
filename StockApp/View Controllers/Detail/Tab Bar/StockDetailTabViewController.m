//
//  StockDetailTabViewController.m
//  StockApp
//
//  The controller for a view responsible for displaying a tabbed view
//  consisting of a stock's status. In particular current status, chart, and
//  competitor summary. This controller also monitors reachability and notifies
//  child views as required.
//
//  Created by David McKnight on 2016-03-04.
//  Copyright (C) 2016 David McKnight actual@oswind.xyz
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#import "Reachability.h"
#import "StockDetailTabViewController.h"
#import "StockDetailViewController.h"
#import "StockCompetitorViewController.h"

#pragma mark - Private declaration

@interface StockDetailTabViewController ()

/* While the view is loaded the reachability may change. This property allows an
 * observer to be set using NSNotificationCenter. */
@property (strong, nonatomic) Reachability * reachabilityUpdates;

/* Checks if the internet is reachable. Returns YES if internet was reachable.
 * Otherwise returns NO. */
- (BOOL) internetIsReachable;

/* Stop the reachabilty notifier and remove this controller from
 * NSNotificationCenter. */
- (void) removeReachabilityObserver;

/* Use this method when using NSNotificationCenter to get reachability status
 * updates. */
- (void) reachabilityDidChange:(NSNotification *)update;

@end

#pragma mark - Public implementation

@implementation StockDetailTabViewController

/* Check if the child-view enables auto-rotation. */
- (BOOL) shouldAutorotate {
    return [self.selectedViewController shouldAutorotate];
}

/* Check the supported orientations of the selected child-view. */
#if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
/* Return type is NSUInteger up to SDK version 8.4. */
- (NSUInteger) supportedInterfaceOrientations {
#else
/* Return type is UIInterfaceOrientationMask starting in SDK version 9. */
- (UIInterfaceOrientationMask) supportedInterfaceOrientations {
#endif
    return [self.selectedViewController supportedInterfaceOrientations];
}

/* Check the preferred orientation of the child-view. */
- (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
    return [self.selectedViewController
            preferredInterfaceOrientationForPresentation];
}

/* Do any additional setup after loading the view. */
- (void)viewDidLoad {
    [super viewDidLoad];
    /* Begin observing for network reachability changes. */
    [self.reachabilityUpdates startNotifier];
    /* Set the UITabBarControllerDelegate. */
    self.delegate = self;
    /* Check the first controller is a StockDetailView. This is a guard against
     * an exception that shouldn't be seen unless the application is changed. */
    if ([self.viewControllers[0]
         isKindOfClass:[StockDetailViewController class]]) {
        StockDetailViewController * detail = self.viewControllers[0];
        detail.context = self.context;
        detail.watchedSymbol = self.watchedSymbol;
    } else {
        NSLog(@"Warning: First controller is not StockDetailViewController.");
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    /* Remove this controller from observing reachability status changes. */
    [self removeReachabilityObserver];
}

/* Dispose of any resources that can be recreated. */
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    [self removeReachabilityObserver]; /* Stop observing reachability. */
}

/* Checks if the internet is reachable. Returns YES if internet was reachable.
 * Otherwise returns NO. */
- (BOOL) internetIsReachable {
    /* Using Apple's Reachability API. */
    Reachability * reachability = self.reachabilityUpdates; /* An alias. */
    /* Get the current network status. */
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    /* Check if networkStatus indicates internet is not reachable. */
    if (networkStatus == NotReachable) {
        return NO; /* Internet not reachable. */
    }
    /* Internet is reachable. No guarantee is made that content can be read. */
    return YES;
}

/* Stop the reachabilty notifier and remove this controller from
 * NSNotificationCenter. */
- (void) removeReachabilityObserver {
    /* Stop the notifier. */
    [self.reachabilityUpdates stopNotifier];
    /* Remove the observer. */
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

/* While the view is loaded the reachability may change. This property allows an
 * observer to be set using NSNotificationCenter. */
- (Reachability *) reachabilityUpdates {
    if (_reachabilityUpdates == nil) {
        _reachabilityUpdates = [Reachability reachabilityForInternetConnection];
        /* Add an observer for network reachability changes. In case the status
         * changes while the view is loaded. */
        [[NSNotificationCenter defaultCenter]
         addObserver:self
         selector:@selector(reachabilityDidChange:)
         name:kReachabilityChangedNotification
         object:nil];
    }
    return _reachabilityUpdates;
}

/* Use this method when using NSNotificationCenter to get reachability status
 * updates. */
- (void) reachabilityDidChange:(NSNotification *)update {
    /* No need to use an object included with the NSNotification as the
     * Reachability object is a local property. Use the updated property to
     * reconfigure the TabBarController and/or child views. */
    StockDetailViewController * detail = self.viewControllers[0];
    [detail updateViewWithReachability:[self internetIsReachable]];
}

#pragma mark - UITabBarControllerDelegate

- (BOOL)tabBarController:(UITabBarController *)tabBarController
        shouldSelectViewController:(UIViewController *)viewController {
    return YES;
}

- (void)tabBarController:(UITabBarController *)tabBarController
        didSelectViewController:(UIViewController *)viewController {
    /* Not doing anything yet. */
}

@end
