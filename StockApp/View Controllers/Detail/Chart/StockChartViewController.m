//
//  StockChartViewController.m
//  StockApp
//
//  The controller for a view responsible for displaying the currently selected
//  stock's chart from the past day's activity.
//
//  Created by David McKnight on 2016-03-04.
//  Copyright (C) 2016 David McKnight actual@oswind.xyz
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#import "StockChartViewController.h"
#import "StockDetailTabViewController.h"
#import "Status.h"
#import "Chart.h"

#pragma mark - Private declaration

@interface StockChartViewController ()

/* The stock's chart showing the day's activity. */
@property (strong, nonatomic) IBOutlet UIImageView *imgChart;

@end

#pragma mark - Public implementation

@implementation StockChartViewController

/* Load the Yahoo webpage per attribution requirements. */
- (IBAction)btnAttribution:(UIButton *)sender {
    [[UIApplication sharedApplication]
            openURL:[NSURL URLWithString:@"https://www.yahoo.com/?ilc=401"]];
}

/* Do any additional setup after loading the view. */
- (void)viewDidLoad {
    [super viewDidLoad];
}

/* Configure the view of chart data. */
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    /* Get the root tab bar controller. */
    StockDetailTabViewController * root =
        (StockDetailTabViewController *)self.tabBarController;
    /* Get the Chart data. */
    Stock * stock = root.watchedSymbol;
    NSData * chartData = stock.stockStatus.statusChart.data;
    /* Setup the view. */
    if (chartData != nil) {
        UIImage * chart = [[UIImage alloc] initWithData:chartData];
        [self.imgChart setImage:chart];
    }
}

/* Dispose of any resources that can be recreated. */
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
