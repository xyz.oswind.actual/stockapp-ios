//
//  StockCompetitorViewController.m
//  StockApp
//
//  The controller for a view responsible for displaying the currently selected
//  stock's competitors summary data.
//
//  Created by David McKnight on 2016-03-04.
//  Copyright (C) 2016 David McKnight actual@oswind.xyz
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#import "StockCompetitorViewController.h"
#import "CompetitorTableViewCell.h"
#import "StockDetailTabViewController.h"
#import "Status.h"
#import "Competitor.h"

#pragma mark - Private declaration

@interface StockCompetitorViewController ()

/* The table view used to display competitor summary data. */
@property (strong, nonatomic) IBOutlet UITableView * competitorTable;

/* The competitor summary data. */
@property (strong, nonatomic) NSArray * competitorData;

@end

#pragma mark - Public implementation

@implementation StockCompetitorViewController

/* Load the Yahoo webpage per attribution requirements. */
- (IBAction)btnAttribution:(UIButton *)sender {
    [[UIApplication sharedApplication]
            openURL:[NSURL URLWithString:@"https://www.yahoo.com/?ilc=401"]];
}

/* Do any additional setup after loading the view. */
- (void)viewDidLoad {
    [super viewDidLoad];
}

/* Dispose of any resources that can be recreated. */
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

/* Configure the competitor table data source. */
- (void)viewWillAppear:(BOOL)animated {
    /* Get the root tab bar controller. */
    StockDetailTabViewController * root =
        (StockDetailTabViewController *)self.tabBarController;
    /* Get the Competitor data. */
    Stock * stock = root.watchedSymbol;
    NSSet * data = stock.stockStatus.statusCompetitors;
    if (data != nil) {
        /* The data of the currently displayed stock for comparison. */
        NSArray * comparison = [NSArray arrayWithObjects:
            stock.stockStatus.symbol, stock.stockStatus.name,
            stock.stockStatus.percentChange, stock.stockStatus.marketCap, nil];
        /* Extract the competitors data from Core Data. */
        NSMutableArray * competitors = [[NSMutableArray alloc] init];
        for (Competitor * c in data) {
            NSArray * extractedCompetitor = [NSArray arrayWithObjects:
                c.symbol, c.name, c.change, c.marketCap, nil];
            [competitors addObject:extractedCompetitor];
        }
        /* Concatenate the comparison and competitors for the table view. */
        [competitors insertObject:comparison atIndex:0];
        /* Update the table view. */
        self.competitorData = competitors;
        [self.competitorTable reloadData];
    }
}

#pragma mark - UITableViewDataSource

/* Configure a table view cell with the competitor data. */
- (UITableViewCell *)tableView:(UITableView *)tableView
                     cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CompetitorTableViewCell * cell = /* Using identifier `competitor`. */
            [tableView dequeueReusableCellWithIdentifier:@"competitor"
                                            forIndexPath:indexPath];
    /* Check if the competitor data is available. */
    if (self.competitorData != nil) {
        /* Obtain cell contents. */
        NSArray * cellContent = self.competitorData[indexPath.row];
        /* Check if the row is the first cell which contains the currently
         * displayed stock. */
        if (indexPath.row == 0) {
            /* Add a border to the first cell which contains the currently 
             * displayed stock. This is purely for ease of comparison to 
             * competitors. */
            cell.layer.borderWidth = 3;
            cell.layer.borderColor = [self.view.tintColor CGColor];
        }
        /* Disable user interaction. */
        cell.userInteractionEnabled = NO;
        /* Setup the cell view. */
        cell.lblSymbol.text = cellContent[0];      /* Symbol label. */
        cell.lblName.text = cellContent[1];        /* Name label. */
        [cell setChangeWithString:cellContent[2]]; /* Change label and image. */
        cell.lblMarketCap.text = cellContent[3];   /* Market cap label. */
    }
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView
             numberOfRowsInSection:(NSInteger)section {
    /* Check if the competitor data is available. */
    return (self.competitorData != nil)? self.competitorData.count: 0;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView
           heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60.0f;
}

@end
