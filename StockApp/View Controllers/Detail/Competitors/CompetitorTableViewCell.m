//
//  CompetitorTableViewCell.m
//  StockApp
//
//  Represents a table view cell to be displayed in a table of competitors.
//
//  Created by David McKnight on 2016-03-04.
//  Copyright (C) 2016 David McKnight actual@oswind.xyz
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#import "CompetitorTableViewCell.h"

#pragma mark - Public implementation

@implementation CompetitorTableViewCell

/* Configure the change label and image views of this cell. */
- (void)setChangeWithString:(NSString *)text {
    /* Get the first character from the change string provided. */
    unichar first = [text characterAtIndex:0];
    /* Check if the change string contains a leading +/-. */
    if (first == '+' || first == '-') {
        if (first == '+') {
            /* Positive change. */
            self.lblChange.textColor = [UIColor greenColor];
            self.imgChange.image = [UIImage imageNamed:@"up"];
        } else if (first == '-') {
            /* Negative change. */
            self.lblChange.textColor = [UIColor redColor];
            self.imgChange.image = [UIImage imageNamed:@"down"];
        }
        /* Set the label text minus the leading +/-. */
        self.lblChange.text = [text substringFromIndex:1];
    } else {
        /* Neither a +/- were found so set a neutral text color. */
        self.lblChange.textColor = [UIColor darkGrayColor];
        /* Neither a +/- were found so set the label text as is. */
        self.lblChange.text = text;
        /* Remove any existing image. */
        self.imgChange.image = nil;
    }
}

/* Prepares a reusable cell for reuse by the table view's delegate. */
- (void)prepareForReuse {
    [super prepareForReuse];
    self.isWatched = NO;
}

/* Initialization code */
- (void)awakeFromNib {
    [super awakeFromNib];
    self.isWatched = NO;
}

/* Configure the view for the selected state */
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
