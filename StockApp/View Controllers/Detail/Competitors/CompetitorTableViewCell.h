//
//  CompetitorTableViewCell.h
//  StockApp
//
//  Represents a table view cell to be displayed in a table of competitors.
//
//  Created by David McKnight on 2016-03-04.
//  Copyright (C) 2016 David McKnight actual@oswind.xyz
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#import <UIKit/UIKit.h>

#pragma mark - Public declaration

@interface CompetitorTableViewCell : UITableViewCell

/* Competitor summary data shown in the containing table view. */
@property (strong, nonatomic) IBOutlet UILabel *lblName;
@property (strong, nonatomic) IBOutlet UIImageView *imgChange;
@property (strong, nonatomic) IBOutlet UILabel *lblChange;
@property (strong, nonatomic) IBOutlet UILabel *lblMarketCap;
@property (strong, nonatomic) IBOutlet UILabel *lblSymbol;

/* Indicates if the competitor represented by this cell is watched. */
@property (nonatomic) BOOL isWatched;

/* Configure the change label and image views of this cell. */
- (void)setChangeWithString:(NSString *)text;

@end
