//
//  StockSearchViewController.m
//  StockApp
//
//  The controller for a view responsible for searching for stocks. Submits the
//  query as required to a StockSearch object and responds to search results
//  by updating the current view.
//
//  Created by David McKnight on 2016-02-10.
//  Copyright (C) 2016 David McKnight actual@oswind.xyz
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#import "Reachability.h"
#import "StockSearchViewController.h"

#pragma mark - Private declaration

@interface StockSearchViewController ()

/* Show an activity indicator when total results is being requested. */
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityTotal;

/* Shows the total number of results found for the query submitted. */
@property (strong, nonatomic) IBOutlet UILabel *labelTotal;

/* Access the search bar text to re-submit a search if the market is changed. */
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;

/* Show an activity indicator when a search has not yet completed. */
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activitySearch;

/* The table view representation of the current symbol lookup query. */
@property (strong, nonatomic) IBOutlet UITableView * searchResults;

/* Stock symbol lookup by description, partial or whole symbol. */
@property (nonatomic,strong) StockSearch * stockSearch;

/* Identify when more results may be available from StockSearch. */
@property (nonatomic) BOOL didGetMoreResultsMessage;

/* Block overlapping requests for more results. */
@property (nonatomic) BOOL moreResultsIsBusy;

/* Indicates when the view is displaying all results possible. */
@property (nonatomic) BOOL hasAllResults;

/* When updating the table view with more rows this stores the previous number
 * of results. When StockSearch returns the new results this previous value
 * is used to insert all new rows in a single batch process. */
@property (nonatomic) NSUInteger previousResultCount;

/* While the view is loaded the reachability may change. This property allows an
 * observer to be set using NSNotificationCenter. */
@property (strong, nonatomic) Reachability * reachabilityUpdates;

/* Update StockSearchView when submitting a string for search. Most importantly
 * this means starting the activity indicator. */
- (void) requestSearchWithText:(NSString *)text;

/* Check if the table view is positioned near the end. This is used to trigger
 * the loading of more results if available. Returns YES if the table view is
 * currently positioned near the end. Otherwise it returns NO. */
- (BOOL) positionIsNearEndInView:(UIScrollView *)view;

/* Reset any state defaults. Most likely this would occur for new stock symbol
 * search requests. */
- (void) resetDefaults;

/* When more results are available update the user interface view elements. */
- (void) onMoreResults;

/* When new symbols need to be added to the table view perform the insertion of
 * new rows in a single batched event. */
- (void) insertNewSymbolsWithCurrentCount:(NSUInteger)current;

/* Stop the reachabilty notifier and remove this controller from
 * NSNotificationCenter. */
- (void) removeReachabilityObserver;

/* Checks if the internet is reachable. Returns YES if internet was reachable.
 * Otherwise returns NO. */
- (BOOL) internetIsReachable;

/* Use this method when using NSNotificationCenter to get reachability status
 * updates. */
- (void) reachabilityDidChange:(NSNotification *)update;

/* Configure the search bar depending on if the internet is reachable or not.
 * Change the placeholder text to `Network Offline` and disable the search bar
 * if the internet is not reachable. Otherwise set it to defaults for search. */
- (void) configureSearchWithReachability:(BOOL)internetReachable;

@end

#pragma mark - Public implementation

@implementation StockSearchViewController

/* Load the Yahoo webpage per attribution requirements. */
- (IBAction)btnAttribution:(UIButton *)sender {
    [[UIApplication sharedApplication]
            openURL:[NSURL URLWithString:@"https://www.yahoo.com/?ilc=401"]];
}

/* When adding stock by searching is cancelled there is nothing to persist. */
- (IBAction)buttonCancel:(id)sender {
    /* Notify interested controllers of the cancellation. */
    [self.delegate searchDidCancel];
}

/* Update the market to search when queries are submitted to stockSearch. */
- (IBAction)didSelectMarket:(UISegmentedControl *)sender {
    if (sender.selectedSegmentIndex == 0) {
        self.stockSearch.queryWorldMarkets = YES; /* Search in world markets. */
    } else if (sender.selectedSegmentIndex == 1) {
        self.stockSearch.queryWorldMarkets = NO;  /* Search in local markets. */
    }
    /* If the search bar contains text re-sumbit the search. */
    [self requestSearchWithText:self.searchBar.text];
}

/* Get the StockSearch object, initializing as required and setting the 
 * delegate to this controller. Search completion and error events will be
 * handled by implementing the StockSearchDelegate protocol. */
- (StockSearch *) stockSearch {
    if (_stockSearch == nil) {
        _stockSearch = [[StockSearch alloc] init];
        _stockSearch.delegate = self;
    }
    return _stockSearch;
}

/* Do any additional setup after loading the view. */
- (void)viewDidLoad {
    [super viewDidLoad];
    /* Configure the search bar. */
    [self.searchBar setReturnKeyType:UIReturnKeyDone];
    [self.searchBar setEnablesReturnKeyAutomatically:NO];
    [self.searchBar setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    /* Modify the activity indicator for total results. Make it smaller. */
    CGAffineTransform scale = CGAffineTransformMakeScale(0.6,0.6);
    self.activityTotal.transform = scale;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    /* Begin observing for network reachability changes. */
    [self.reachabilityUpdates startNotifier];
    /* Set defaults. */
    [self resetDefaults];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    /* Remove this controller from observing reachability status changes. */
    [self removeReachabilityObserver];
}

/* Reset any state defaults. Most likely this would occur for new stock symbol
 * search requests. */
- (void) resetDefaults {
    self.didGetMoreResultsMessage = NO;
    self.moreResultsIsBusy = NO;
    self.hasAllResults = NO;
    self.previousResultCount = 0;
}

/* Dispose of any resources that can be recreated. */
- (void)didReceiveMemoryWarning {
    self.searchBar.text = @""; /* Clear the current search bar text. */
    self.stockSearch.delegate = nil;   /* Allow StockSearch to unload. */
    self.stockSearch = nil;            /* Allow StockSearch to unload. */
    [self removeReachabilityObserver]; /* Stop observing reachability. */
    [super didReceiveMemoryWarning];
}

/* Stop the reachabilty notifier and remove this controller from
 * NSNotificationCenter. */
- (void) removeReachabilityObserver {
    /* Stop the notifier. */
    [self.reachabilityUpdates stopNotifier];
    /* Remove the observer. */
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

/* Checks if the internet is reachable. Returns YES if internet was reachable.
 * Otherwise returns NO. */
- (BOOL) internetIsReachable {
    /* Using Apple's Reachability API. */
    Reachability * reachability = self.reachabilityUpdates; /* An alias. */
    /* Get the current network status. */
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    /* Check if networkStatus indicates internet is not reachable. */
    if (networkStatus == NotReachable) {
        return NO; /* Internet not reachable. */
    }
    /* Internet is reachable. No guarantee is made that content can be read. */
    return YES;
}

/* While the view is loaded the reachability may change. This property allows an
 * observer to be set using NSNotificationCenter. */
- (Reachability *) reachabilityUpdates {
    if (_reachabilityUpdates == nil) {
        _reachabilityUpdates = [Reachability reachabilityForInternetConnection];
        /* Add an observer for network reachability changes. In case the status
         * changes while the view is loaded. */
        [[NSNotificationCenter defaultCenter]
         addObserver:self
         selector:@selector(reachabilityDidChange:)
         name:kReachabilityChangedNotification
         object:nil];
    }
    return _reachabilityUpdates;
}

/* Use this method when using NSNotificationCenter to get reachability status
 * updates. */
- (void) reachabilityDidChange:(NSNotification *)update {
    /* No need to use an object included with the NSNotification as the
     * Reachability object is a local property. Use the updated property to
     * reconfigure the search bar. */
    [self configureSearchWithReachability:[self internetIsReachable]];
    /* Stop the activity indicators if the connection is now offline. */
    if (![self internetIsReachable]) {
        [self.activitySearch stopAnimating];
        [self.activityTotal stopAnimating];
    }
}

/* Configure the search bar depending on if the internet is reachable or not.
 * Change the placeholder text to `Network Offline` and disable the search bar
 * if the internet is not reachable. Otherwise set it to defaults for search. */
- (void) configureSearchWithReachability:(BOOL)internetReachable {
    /* Use internetReachable to configure the search bar. */
    if (internetReachable == YES) {
        /* Enable search. */
        [self.searchBar setPlaceholder:@"Symbol or description"];
        [self.searchBar setUserInteractionEnabled:YES];
    } else {
        /* Disable search. */
        self.searchBar.text = @""; /* Clear the current search bar text. */
        [self.searchBar setPlaceholder:@"Network Offline"];
        [self.searchBar setUserInteractionEnabled:NO];
    }
}

/* Update StockSearchView when submitting a string for search. Most importantly
 * this means starting the activity indicator. */
- (void) requestSearchWithText:(NSString *)text {
    if (![text isEqualToString:@""]) {
        /* Reset defaults. */
        [self resetDefaults];
        [self.activitySearch startAnimating]; /* Start activity indication. */
        [self.stockSearch performSearchWithString:text]; /* Submit query. */
    }
}

/* Check if the table view is positioned near the end. This is used to trigger
 * the loading of more results if available. Returns YES if the table view is
 * currently positioned near the end. Otherwise it returns NO. */
- (BOOL) positionIsNearEndInView:(UIScrollView *)view {
    /* The current position scrolled in the available scrollable space. */
    float currentPosition = view.contentOffset.y + view.bounds.size.height;
    /* The size of the table view taking into account unscrolled space. */
    float tableViewSize = view.contentSize.height;
    float loadMore = 160; /* Distance from table end to load more. */
    if(currentPosition + loadMore > tableViewSize) {
        return YES; /* Current position is near the end of the view. */
    }
    return NO; /* Current position is not near the end of the view. */
}

#pragma mark - UISearchBarDelegate

/* Editing should begin when the search bar is selected. */
- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    return YES;
}

/* Request a search with the currently entered search text. */
- (void)searchBar:(UISearchBar *)searchBar
    textDidChange:(NSString *)searchText {
    [self requestSearchWithText:searchText];
}

/* Dismiss the keyboard when enter or search is pressed. */
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    /* The search button `Done` has been selected. Dismiss the keyboard. */
    [searchBar resignFirstResponder];
}

#pragma mark - StockSearchDelegate

/* A query which was previously submitted has completed. The parameter `symbols`
 * contains the results as symbol-description pairs. */
- (void) searchDidCompleteWithSymbols:(NSArray *)symbols {
    [self.activitySearch stopAnimating]; /* Stop activity indication. */
    /* Update the local view of the query results. */
    if (symbols != nil) {
        /* Update the table view representation of the results. */
        [self.searchResults reloadData];
        /* Re-position the table view to the top. */
        if (symbols.count > 0) {
            [self.searchResults scrollToRowAtIndexPath:
                [NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:
                    UITableViewScrollPositionTop animated:NO];
        }
        /* Start the activity indicator for total results. */
        [self.activityTotal startAnimating];
    }
}

/* A query which was previously submitted did not complete. The parameter
 * `error` contains the cause of the failure. */
- (void) searchDidNotCompleteWithError:(NSError *)error {
    NSLog(@"A session failed with client-side error code: %ld, "
          "and description: %@", (long)error.code,
          error.localizedDescription);
    [self.activitySearch stopAnimating]; /* Stop activity indication. */
}

/* A query which was previously submitted now has a total number of results.
 * This may exceed the number of results previously seen when receiving the
 * searchDidCompleteWithSymbols message. This message will be sent when the
 * total number of results exceeds the maximum returned per request. For Yahoo
 * this is defined in StockSearch as YAHOO_MAX_PER_REQUEST. */
- (void) didGetTotalResultsForLastQuery:(NSString *)total {
    /* Update the total results label using the total results found remotely. */
    self.labelTotal.text = [NSString stringWithFormat:@"%@ found", total];
    /* Check if this is a message indicating more results than displayed. */
    if (self.stockSearch.totalResults > YAHOO_MAX_PER_REQUEST) {
        self.didGetMoreResultsMessage = YES;
    }
    /* Stop the activity indicator. */
    [self.activityTotal stopAnimating];
}

/* A query which was previously submitted already has a total number of results.
 * This message will be sent when the total number of results is less than the
 * maximum returned per request. For Yahoo this is defined in StockSearch as
 * YAHOO_MAX_PER_REQUEST. The symbols previously returned represent the total of
 * results found for the last query. */
- (void) didAlreadyGetTotalResultsForLastQuery {
    /* Update the total results label using the number of symbols. */
    self.labelTotal.text = [NSString stringWithFormat:@"%lu found",
                            (unsigned long)self.stockSearch.symbols.count];
    /* Stop the activity indicator. */
    [self.activityTotal stopAnimating];
}

/* A request for more results has completed. The property `symbols` associated
 * with this instance of StockSearch reflects the current view of total symbols.
 * The updated view of total symbols includes all previous symbols with new
 * symbols appended to the end. Interested controllers should update their view
 * with the new entries. */
- (void) moreResultsDidCompleteWithMore {
    [self onMoreResults];
}

/* A request for more results has completed without any changes to the property
 * `symbols` associated with this instance of StockSearch. Most likely this
 * means all the symbols are already loaded and any request for more is out-of-
 * bounds. Interested controllers can use this to indicate no further stock
 * symbols can be displayed.*/
- (void) moreResultsDidComplete {
    self.hasAllResults = YES;
    [self onMoreResults];
}

/* When more results are available update the user interface view elements. */
- (void) onMoreResults {
    [self.activitySearch stopAnimating]; /* Stop the activity indicator. */
    self.moreResultsIsBusy = NO; /* Re-enable more results check. */
    /* Check how many results have now been processed. */
    NSUInteger currentResultCount = self.stockSearch.symbols.count;
    /* Check if new rows need to be inserted. */
    if (currentResultCount > self.previousResultCount) {
        /* Insert the new rows. */
        [self insertNewSymbolsWithCurrentCount:currentResultCount];
    }
}

/* When new symbols need to be added to the table view perform the insertion of
 * new rows in a single batched event. */
- (void) insertNewSymbolsWithCurrentCount:(NSUInteger)current {
    /* Batch all the index paths for inserted rows together. */
    NSMutableArray * insertResults = [[NSMutableArray alloc] init];
    /* Begin gathering index paths. */
    [self.searchResults beginUpdates];
    for (NSUInteger i = self.previousResultCount; i < current; i++) {
        /* Create an index path for each new row. */
        [insertResults addObject:
         [NSIndexPath indexPathForRow:i inSection:0]];
    }
    /* Add the new index paths. */
    [self.searchResults insertRowsAtIndexPaths:insertResults
                            withRowAnimation:UITableViewRowAnimationAutomatic];
    /* Finished updates. */
    [self.searchResults endUpdates];
}

#pragma mark - UITableViewDataSource

/* The number of stock symbols found for the current symbol lookup query. */
- (NSInteger)tableView:(UITableView *)tableView
             numberOfRowsInSection:(NSInteger)section {
    /* If stocks is not currently set there are no symbols to show. */
    if (self.stockSearch.symbols == nil) {
        return 0; /* No symbols. */
    }
    /* Return the number of symbols for the current symbol lookup query. */
    return self.stockSearch.symbols.count;
}

/* Setup a cell in the table view using available symbols and descriptions. */
- (UITableViewCell *)tableView:(UITableView *)tableView
                     cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    /* Get a reuseable cell by identifier `search`. */
    UITableViewCell *cell = [tableView
                             dequeueReusableCellWithIdentifier:@"search"
                             forIndexPath:indexPath];
    /* Obtain the stock symbol. */
    NSString * symbol = [self.stockSearch getSymbolAtIndex:indexPath.row];
    /* Obtain the symbol description. */
    NSString * desc = [self.stockSearch getDescriptionAtIndex:indexPath.row];
    /* Set the cell view labels. */
    cell.textLabel.text = symbol;
    cell.detailTextLabel.text = desc;
    /* Change the cell label color. */
    cell.textLabel.textColor = [self.view tintColor];
    cell.detailTextLabel.textColor = [self.view tintColor];
    
    return cell;
}

#pragma mark - UITableViewDelegate

/* When a table view row is selected indicate to Core Data that the symbol
 * should be persisted and displayed on the view Stock Watch. */
- (void)tableView:(UITableView *)tableView
        didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    /* Get the selected cell contents. */
    UITableViewCell * cell =
        [self.searchResults cellForRowAtIndexPath:indexPath];
    /* Use the cell contents to request a managed Stock object. */
    NSString * symbol = cell.textLabel.text;     /* Stock symbol. */
    NSString * desc = cell.detailTextLabel.text; /* Stock description. */
    /* Notify any interested controllers that the search view is done. */
    [self.delegate searchDidFinishWithSymbol:symbol andDescription:desc];
}

#pragma mark - UIScrollViewDelegate

/* Trigger the loading of more results into the table view when the table is
 * scrolled to near the end of scrollable space. The loading of more results 
 * will only occur if more results are available than are currently loaded into
 * the table view. */
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    /* Check if the delegate message didGetTotalResultsForLastQuery has been 
     * sent with more results. If NO then it is known that more results do not 
     * exist. If YES then check for more as the table scrolls. */
    if (!self.hasAllResults &&         /* Not already displaying all results. */
        self.didGetMoreResultsMessage &&         /* Got more results message. */
        !self.moreResultsIsBusy &&            /* Not waiting on more results. */
        self.stockSearch.totalResults >                   /* More to display. */
        [[self searchResults] numberOfRowsInSection:0] && /* More to display. */
        [self positionIsNearEndInView:scrollView]) {      /* Near end. */
        /* The table view is scrolled near the end. Request more results and
         * update the table view with the new rows. */
        self.moreResultsIsBusy = YES;          /* Block overlapping requests. */
        [self.activitySearch startAnimating];  /* Start activity indicator. */
        /* Hang onto the current number of results for a batch update. */
        self.previousResultCount = self.stockSearch.symbols.count;
        [self.stockSearch requestNextResults]; /* Query for more results. */
    }
}

@end
