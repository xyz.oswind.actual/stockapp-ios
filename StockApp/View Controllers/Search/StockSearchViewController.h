//
//  StockSearchViewController.h
//  StockApp
//
//  The controller for a view responsible for searching for stocks. Submits the
//  query as required to a StockSearch object and responds to search results
//  by updating the current view.
//
//  Created by David McKnight on 2016-02-10.
//  Copyright (C) 2016 David McKnight actual@oswind.xyz
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#import <UIKit/UIKit.h>
#import "StockSearchViewDelegate.h"
#import "StockSearch.h"
#import "Stock.h"

#pragma mark - Public declaration

@interface StockSearchViewController : UIViewController
<StockSearchDelegate, UISearchBarDelegate, UITableViewDelegate,
    UITableViewDataSource, UIScrollViewDelegate>

/* Notify a controller that implements the StockSearchViewDelegate protocol when
 * a stock symbol search is complete or cancelled. */
@property (nonatomic, strong) id <StockSearchViewDelegate> delegate;

@end
