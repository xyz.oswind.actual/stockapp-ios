//
//  StockSearchViewDelegate.h
//  StockApp
//
//  Declares the protocol used when a controller is interested in symbols which
//  are to be persisted from those found in StockSearchView. Interested
//  controllers use this protocol to indicate new watched stocks from search
//  results and detecting cancellation.
//
//  Created by David McKnight on 2016-02-24.
//  Copyright (C) 2016 David McKnight actual@oswind.xyz
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#import <Foundation/Foundation.h>
#import "Stock.h"

@protocol StockSearchViewDelegate <NSObject>

/* A stock has been selected in StockSearchView which should be persisted in
 * Core Data. The parameter `symbol` contains the selected stock symbol and
 * `description` contains the stock company name. Delegate controllers should 
 * check if the symbol is already watched. If not then create a new managed 
 * object of type Stock to persist the watched stock data. */
- (void) searchDidFinishWithSymbol:(NSString *)symbol
                    andDescription:(NSString *)description;

/* StockSearchView has been cancelled without selecting a stock to watch. */
- (void) searchDidCancel;

@end
